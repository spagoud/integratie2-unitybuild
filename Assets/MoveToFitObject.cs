﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToFitObject : MonoBehaviour
{

    public Transform centerPoint;

    public void Update()
    {
        if (centerPoint != null)
        {
            transform.position = centerPoint.position;
            transform.position += Vector3.up * 10;
        }
    }
}
