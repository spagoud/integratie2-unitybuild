﻿using Assets.Scripts.Connectors.SocketConnections;
using UnityEngine;
using UnityEngine.UI;

public class LoginForm : MonoBehaviour
{
    public InputField Username;
    public InputField Password;

    public Text ErrorTextField;

    // Start is called before the first frame update
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {

    }
    
    public void Login()
    {
        if (!(string.IsNullOrWhiteSpace(Username.text) || string.IsNullOrWhiteSpace(Password.text)))
        {
            var sender = AngularSenderController.GetInstance();
            sender.LogIn(Username.text,
                Password.text,
                (user)=> {
                    SceneController.GetInstance().LoadNextScene();
                },
                (error)=> {
                    ErrorTextField.text = error; ErrorTextField.gameObject.SetActive(true);
                });
        }
    }

    public void Register()
    {
        AngularSenderController.GetInstance().Register();
    }
}
