mergeInto(LibraryManager.library, {
  StartGame: function () {
    window.StartGame();
  },StopGame: function () {
    window.StopGame();
  },OpenDoor: function (doorAction) {
    window.OpenDoor(Pointer_stringify(doorAction));
  },GetRange: function (heroName) {
    window.GetRange(Pointer_stringify(heroName));
  },DoMove: function (movePath) {
    window.DoMove(Pointer_stringify(movePath));
  },ChatRequest: function (chatMessage) {
    window.ChatRequest(Pointer_stringify(chatMessage));
  },OpenChest: function (chestAction) {
    window.OpenChest(Pointer_stringify(chestAction));
  },AttackMonster: function (attackAction) {
    window.AttackMonster(Pointer_stringify(attackAction));
  }
});