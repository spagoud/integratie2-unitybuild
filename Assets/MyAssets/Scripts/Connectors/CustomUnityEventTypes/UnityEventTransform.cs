﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class UnityEventTransform : UnityEvent<Transform> { }

