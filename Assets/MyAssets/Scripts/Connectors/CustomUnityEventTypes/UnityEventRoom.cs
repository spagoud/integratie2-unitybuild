﻿using Assets.Scripts.Model;
using System;
using UnityEngine.Events;

[Serializable]
public class UnityEventGameRoom : UnityEvent<Room> { }

