﻿using Assets.Scripts.Model;
using System;
using System.Collections.Generic;
using Assets.Scripts.Model.Messages;
using UnityEngine.Events;

[Serializable]
public class UnityEventTurn : UnityEvent<MessageBase> { }