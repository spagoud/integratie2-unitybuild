﻿using System;
using Assets.Scripts.Model.Messages;
using UnityEngine.Events;

[Serializable]
public class UnityEventAttack : UnityEvent<AttackMessage> { }