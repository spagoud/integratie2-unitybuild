﻿using Assets.Scripts.Model;
using System;
using UnityEngine.Events;

[Serializable]
public class UnityEventGame : UnityEvent<Game> { }
