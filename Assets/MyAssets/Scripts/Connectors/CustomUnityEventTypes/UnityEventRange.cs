﻿using Assets.Scripts.Model;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

[Serializable]
public class UnityEventRange : UnityEvent<List<Tile>> { }
