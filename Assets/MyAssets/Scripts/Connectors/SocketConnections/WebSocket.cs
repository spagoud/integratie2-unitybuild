﻿using UnityEngine;
using WebSocketClient;
using WebSocketSharp;

namespace Assets.Scripts.Connectors.SocketConnections
{
    public class MyWebSocket
    {
        //private string uri = "ws://integratie2-broadcaster.herokuapp.com/ws/websocket";
        private string uri = "ws://localhost:8082/ws/websocket";


        private StompMessageSerializer serializer = new StompMessageSerializer();
        private AngularRecieverController recieverController;
        private WebSocket ws;

        public MyWebSocket(AngularRecieverController recieverController)
        {
            this.recieverController = recieverController;
            ws =  new WebSocket(uri);
        }

        public void Disconnect()
        {
            ws.Close();
        }

        public void Connect(long lobbyId)
        {
            ws.OnOpen += (sender, e) =>
            {
                var connect = new StompMessage("CONNECT");
                connect["accept-version"] = "1.1";
                connect["heart-beat"] = "10000,10000";
                ws.Send(serializer.Serialize(connect));



                var sub0 = new StompMessage("SUBSCRIBE");
                sub0["id"] = "sub-0";
                sub0["destination"] = "/lobby/chat/" + lobbyId;
                ws.Send(serializer.Serialize(sub0));

                var sub1 = new StompMessage("SUBSCRIBE");
                sub1["id"] = "sub-1";
                sub1["destination"] = "/lobby/" + lobbyId + "/event";
                ws.Send(serializer.Serialize(sub1));

                var sub2 = new StompMessage("SUBSCRIBE");
                sub2["id"] = "sub-2";
                sub2["destination"] = "/lobby/" + lobbyId + "/game";
                ws.Send(serializer.Serialize(sub2));

            };

            ws.OnError += (sender, e) =>
             Debug.Log("Error: " + e.Message);
            ws.OnMessage += (sender, e) =>
            {
                var message = serializer.Deserialize(e.Data);
                switch (message.Headers["id"])
                {
                    case "sub-0": recieverController.ChatMessageResponse(message.Body); break;
                    case "sub-1": recieverController.ActionMessage(message.Body); break;
                    case "sub-2": recieverController.StartGameResponce(message.Body); break;
                    default: Debug.Log(message); break;
                }
                ;
            };

            ws.Connect();
        }

    }

}
