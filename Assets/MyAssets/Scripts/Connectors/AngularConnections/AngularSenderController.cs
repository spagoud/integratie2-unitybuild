﻿using Assets.Scripts.Connectors.WebConnectors;
using Assets.Scripts.Model;
using Assets.Scripts.WebConnectors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Assets.Scripts.Model.Actions.Sending;
using Assets.Scripts.Model.Messages;
using UnityEngine;
using Newtonsoft.Json;

[RequireComponent(typeof(AngularRecieverController))]
public class AngularSenderController : MonoBehaviour, ISenderController
{
    
    private static AngularSenderController instance;
    public static AngularSenderController GetInstance()
    {
        return instance;
    }

#if UNITY_WEBGL
    //Properties for WEBGL

    [DllImport("__Internal")]
    private static extern void StartGame();

    [DllImport("__Internal")]
    private static extern void StopGame();

    [DllImport("__Internal")]
    private static extern void OpenDoor(string doorAction);

    [DllImport("__Internal")]
    private static extern void GetRange(string heroName);

    [DllImport("__Internal")]
    private static extern void DoMove(string movePath);

    [DllImport("__Internal")]
    private static extern void ChatRequest(string chatMessage);

    [DllImport("__Internal")]
    private static extern void OpenChest(string chestAction);

    [DllImport("__Internal")]
    private static extern void AttackMonster(string attackAction);

#elif (UNITY_EDITOR&&!UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
    //Properties for Unity editor and mobile

    private WebSenderController webSenderController;

#endif

    void Start()
    {
        instance = this;

#if (UNITY_EDITOR&&!UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        webSenderController = new WebSenderController(GetComponent<AngularRecieverController>());
#endif
    }

    public void StopGameRequest()
    {
#if (UNITY_EDITOR && !UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        webSenderController.StopGame();
#elif UNITY_WEBGL

        StopGame();
#endif
    }

    public void StartGameRequest()
    {
#if (UNITY_EDITOR&&!UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        webSenderController.StartGameRequest(1);
#elif UNITY_WEBGL

        StartGame();
#endif

    }

    public void LogIn(string username, string password, Action<User> success, Action<string> error)
    {
#if (UNITY_EDITOR&&!UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        StartCoroutine(webSenderController.LogIn(username,
            password, 
            (User user) => {
                Debug.Log(String.Format("User {0} successfully logged in!", user.Username));
                success.Invoke(user);
            }, 
            (errorMsg) => {
                Debug.Log(String.Format("User failed to logged in! Error: {0}", error));
                error.Invoke(errorMsg);
            }));
#endif
    }
    internal void Register()
    {
#if (UNITY_EDITOR&&!UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        webSenderController.RegisterRedirect();
#endif
    }
    
    public void OpenDoorRequest(DoorAction doorAction)
    {
#if (UNITY_EDITOR&&!UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        string json = JsonConvert.SerializeObject(doorAction);
        Debug.Log("dooraction: " + json);
        webSenderController.OpenDoorRequest(json);
#elif UNITY_WEBGL
        string json = JsonConvert.SerializeObject(doorAction);
        Debug.Log("dooraction: " + json);
        OpenDoor(json);
#endif

    }

    public void GetRangeRequest(string heroName)
    {
#if (UNITY_EDITOR&&!UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        webSenderController.GetRangeRequest(json);
#elif UNITY_WEBGL
        Debug.Log("GetRangeRequest: " + heroName);
        GetRange(heroName);
#endif

    }

    public void MoveRequest(MoveAction moveAction)
    {
#if (UNITY_EDITOR&&!UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        webSenderController.MoveRequest(json);
#elif UNITY_WEBGL
        string json = JsonConvert.SerializeObject(moveAction);
        Debug.Log("MoveRequest: " + json);
        DoMove(json);
#endif

    }

    public void ChatMessageRequest(ChatMessage chatMessage)
    {
#if (UNITY_EDITOR && !UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        webSenderController.ChatRequest(ChatMessage);
#elif UNITY_WEBGL
        string json = JsonConvert.SerializeObject(chatMessage);
        Debug.Log("ChatMessage: " + json);
        ChatRequest(json);
#endif
    }

    public void OpenChestRequest(ChestAction chestAction)
    {
#if (UNITY_EDITOR&&!UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        webSenderController.OpenChestRequest(json);
#elif UNITY_WEBGL
        string json = JsonConvert.SerializeObject(chestAction);
        Debug.Log("OpenChestRequest: " + json);
        OpenChest(json);
#endif

    }

    public void AttackRequest(AttackAction attackAction)
    {
#if (UNITY_EDITOR&&!UNITY_WEBGL) || UNITY_IOS || UNITY_ANDROID
        webSenderController.AttackRequest(json);
#elif UNITY_WEBGL
        string json = JsonConvert.SerializeObject(attackAction);
        Debug.Log("AttackRequest: " + json);
        AttackMonster(json);
#endif

    }
}
