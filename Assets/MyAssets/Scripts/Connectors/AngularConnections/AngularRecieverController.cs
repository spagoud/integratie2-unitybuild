﻿using Assets.Scripts.Model;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Assets.Scripts.Model.Enums;
using Assets.Scripts.Model.Messages;
using UnityEngine;
using UnityEngine.Events;

public class AngularRecieverController : MonoBehaviour
{
    private static AngularRecieverController instance;

    public static AngularRecieverController GetInstance()
    {
        return instance;
    }


    public static UnityEventGame GameStartedEvent = new UnityEventGame();
    public static UnityEventRange RangeEvent = new UnityEventRange();
    public static UnityEventMove MoveEvent = new UnityEventMove();
    public static UnityEventOpenDoor OpenDoorEvent = new UnityEventOpenDoor();
    public static UnityEventSequence SequenceEvent = new UnityEventSequence();
    public static UnityEventTurn Turnevent = new UnityEventTurn();
    public static UnityEventUser SetUserEvent = new UnityEventUser();
    public static UnityEventTrap TrapEvent = new UnityEventTrap();
    public static UnityEventChat ChatEvent = new UnityEventChat();
    public static UnityEventChest ChestEvent = new UnityEventChest();
    public static UnityEventAttack AttackEvent = new UnityEventAttack();
    public static UnityEventEndGame EndGameEvent = new UnityEventEndGame();
    public static bool Ready = true;
    private Queue<string> EventQueue = new Queue<string>();

    private JsonSerializerSettings jsonSerializerSetting = new JsonSerializerSettings();

    public void Start()
    {
        instance = this;
        jsonSerializerSetting.MissingMemberHandling = MissingMemberHandling.Ignore;
        StartCoroutine(CheckForReadyEvent());
        //TestBuffer();
    }

    public void TestBuffer()
    {
        new Thread(() =>
        {
            Thread.CurrentThread.IsBackground = true;
            Thread.Sleep(10000);
            Debug.Log("TestBuffer 10");
            string turnSequenceJson =
                "{\"actor\":null,\"actionType\":\"TURNSEQUENCE\",\"playerSequence\":[\"JozanAI\",\"LiddaAI\",\"Regdar\",\"MialeeAI\"]}";
            var turnJson = "{\"actor\":\"LiddaAI\",\"actionType\":\"TURN\"}";
            //EventReceive(turnSequenceJson);
            EventReceive(turnJson);
            Debug.Log(EventQueue.Count);
            Thread.Sleep(10000);
            var turnJson2 = "{\"actor\":\"Regdar\",\"actionType\":\"TURN\"}";
            EventReceive(turnJson2);
        }).Start();
    }

    public void StartGameResponce(string json)
    {
        Console.WriteLine(json);
        Game game = JsonConvert.DeserializeObject<Game>(json);
        GameStartedEvent.Invoke(game);
        Ready = true;
    }

    public void SetUser(string json)
    {
        Console.WriteLine(json);
        SetUserEvent.Invoke(json);
    }

    public void GetRangeResponse(string json)
    {
        Console.WriteLine(json);
        List<Tile> range = JsonConvert.DeserializeObject<List<Tile>>(json);
        RangeEvent.Invoke(range);
    }

    public void EventReceive(string json)
    {
        Console.WriteLine(json);
        EventQueue.Enqueue(json);
    }

    public void EndGame(string json)
    {
        Console.WriteLine(json);
        bool end = JsonConvert.DeserializeObject<bool>(json);
        EndGameEvent.Invoke(end);
    }

    private IEnumerator CheckForReadyEvent()
    {
        while (true)
        {
            yield return new WaitUntil(() => Ready && EventQueue.Count > 0);
                Ready = false;
                EventHandler(EventQueue.Dequeue());
                //Debug.Log("Check For Event Queue");
                //Debug.Log(EventQueue.Count);
        }
    }

    private void EventHandler(string json)
    {
        MessageBase messageBase = JsonConvert.DeserializeObject<MessageBase>(json, jsonSerializerSetting);
        switch (messageBase.MessageType)
        {
            case MessageType.MOVE:
                Console.WriteLine("Move action unity");
                MoveMessage moveMessage = JsonConvert.DeserializeObject<MoveMessage>(json);
                MoveEvent.Invoke(moveMessage);
                break;
            case MessageType.TURNSEQUENCE:
                Console.WriteLine("turnsequence unity");
                SequenceMessage sequenceMessage = JsonConvert.DeserializeObject<SequenceMessage>(json);
                SequenceEvent.Invoke(sequenceMessage);
                break;
            case MessageType.TURN:
                Console.WriteLine("Turn unity");
                Turnevent.Invoke(messageBase);
                break;
            case MessageType.DOOR:
                Console.WriteLine("Door unity");
                DoorMessage doorMessage = JsonConvert.DeserializeObject<DoorMessage>(json);
                OpenDoorEvent.Invoke(doorMessage);
                break;
            case MessageType.TRAP:
                Console.WriteLine("Trap unity");
                TrapMessage trapMessage = JsonConvert.DeserializeObject<TrapMessage>(json);
                TrapEvent.Invoke(trapMessage);
                break;
            case MessageType.CHEST:
                Console.WriteLine("Chest unity");
                ChestMessage chestMessage = JsonConvert.DeserializeObject<ChestMessage>(json);
                ChestEvent.Invoke(chestMessage);
                break;
            case MessageType.ATTACK:
                Console.WriteLine("Attack unity");
                AttackMessage attackMessage = JsonConvert.DeserializeObject<AttackMessage>(json);
                AttackEvent.Invoke(attackMessage);
                break;
        }
    }

    internal void ActionMessage(string body)
    {
        throw new NotImplementedException();
    }

    internal void ChatMessageResponse(string json)
    {
        Console.WriteLine(json);
        ChatMessage chatMessage = JsonConvert.DeserializeObject<ChatMessage>(json);
        ChatEvent.Invoke(chatMessage);
    }
}