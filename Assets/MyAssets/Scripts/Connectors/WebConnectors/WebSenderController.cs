﻿using Assets.Scripts.Connectors.SocketConnections;
using Assets.Scripts.Model;
using Assets.Scripts.WebConnectors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Model.Actions.Sending;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Connectors.WebConnectors
{
    class WebSenderController
    {
        private readonly string BACKENDURL = "http://localhost:8081/";
        //private readonly string BACKENDURL = "https://integratie2-dev-backend2019.herokuapp.com/";

        private readonly string FRONTENDURL = "http://localhost:4200/";
        //private readonly string FRONTENDURL = "https://integratie2-dev-frontend2019.herokuapp.com/";

        private User user = null;
        //private int Lobby_Id = 1;
        public AngularRecieverController RecieverController { get; set; }
        private HeaderAttribute Authorization;


        private MyWebSocket socket;


        public WebSenderController(AngularRecieverController recieverController)
        {
            this.socket = new MyWebSocket(recieverController);
            this.RecieverController = recieverController;
        }

        public IEnumerator StartGameRequest(long lobbyId)
        {
            this.socket.Connect(lobbyId);
            using (UnityWebRequest webClient = UnityWebRequest.Get(BACKENDURL + "lobby/" + lobbyId + "/start"))
            {
                yield return webClient.SendWebRequest();
            }
        }

        public IEnumerator StopGame()
        {
            using (UnityWebRequest webClient = UnityWebRequest.Get(BACKENDURL + "lobby/start"))
            {
                yield return webClient.SendWebRequest();
            }
        }

        public IEnumerator LogIn(string username, string password, Action<User> success, Action<string> error)
        {
            WWWForm formData = new WWWForm();
            formData.AddField("username", username);
            formData.AddField("password", password);
            using (UnityWebRequest webClient = UnityWebRequest.Post(BACKENDURL + "login", formData))
            {
                yield return webClient.SendWebRequest();
                if (webClient.isHttpError || webClient.isNetworkError)
                {
                    if (webClient.responseCode == 401)
                    {
                        error.Invoke("Wrong Credentials");
                    }
                    else
                    {
                        error.Invoke("Unknown Error");
                    }
                }
                else
                {
                    var user = new User()
                    {
                        Username = webClient.GetResponseHeader("username"),
                        Token = webClient.GetResponseHeader("Authorization")
                    };
                    this.user = user;
                    success.Invoke(user);

                }
            }
        }

        /*public IEnumerator OpenDoorRequest(string doorAction)
        {
            //TODO: error handling
            Debug.Log("websender: "+doorAction);
            using (UnityWebRequest webClient = UnityWebRequest.Post(BACKENDURL + "action/lobby/" + Lobby_Id + "/openDoor",doorAction))
            {
                yield return webClient.SendWebRequest();
            }
        }*/
        /*
        public IEnumerator GetRangeRequest( )
        {

            using (UnityWebRequest webClient = UnityWebRequest.Post(BACKENDURL + "movement/range/"))
            {
                yield return webClient.SendWebRequest();
            }
        }*/

        internal void RegisterRedirect()
        {
            Application.OpenURL(FRONTENDURL + "register");
        }


        public IEnumerator ChatRequest(string chatMessage)
        {
            throw new NotImplementedException();
        }
    }
}
