﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model
{
    public class Trap
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("dismanteled")]
        public Boolean Dismantled { get; set; }

        [JsonProperty("position")]
        public Tile Position { get; set; }
    }
}
