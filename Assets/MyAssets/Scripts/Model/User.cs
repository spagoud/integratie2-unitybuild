﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model
{
    public class User
    {
        public string Username { get; set; }
        public string Token { get; set; }
    }
}
