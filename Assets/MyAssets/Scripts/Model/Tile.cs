﻿using Assets.Scripts.Model.Enums;
using Newtonsoft.Json;

namespace Assets.Scripts.Model
{

    public class Tile
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("x")]
        public int X { get; set; }

        [JsonProperty("y")]
        public int Y { get; set; }

        [JsonProperty("tileType")]
        public TileType TileType { get; set; }

        [JsonProperty("pillar")]
        public bool Pillar { get; set; }

        [JsonProperty("chest")]
        public bool Chest { get; set; }

        [JsonProperty("trapType")]
        public TrapType TrapType { get; set; }

        [JsonProperty("door")]
        public bool Door { get; set; }

        public Tile(TileType type, int x, int y)
        {
            TileType = type;
            X = x;
            Y = y;
        }
    }
}
