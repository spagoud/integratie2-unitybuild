﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Messages
{
    public class TradeMessage:MessageBase
    {
        [JsonProperty("receiver")]
        public string Receiver { get; set; }

        [JsonProperty("toSender")]
        public List<Item> ToSender { get; set; }

        [JsonProperty("toReceiver")]
        public List<Item> ToReveiver { get; set; }

    }
}
