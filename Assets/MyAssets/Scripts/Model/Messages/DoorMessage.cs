﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Messages
{
    public class DoorMessage: MessageBase
    {
        [JsonProperty("newRoom")]
        public Room NewRoom { get; set; }
        [JsonProperty("doorId")]
        public long DoorId { get; set; }
    }
}
