﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Model.Enums;

namespace Assets.Scripts.Model.Messages
{
    public class ChatMessage
    {
        [JsonProperty("sender")]
        public string Sender { get; set; }
        [JsonProperty("chatMessageType")]
        public ChatMessageType ChatMessageType { get; set; }
        [JsonProperty("content")]
        public string Content { get; set; }
    }
}
