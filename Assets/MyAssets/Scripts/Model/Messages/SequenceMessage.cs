﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Scripts.Model.Messages
{
    public class SequenceMessage : MessageBase
    {
        [JsonProperty("playerSequence")] public List<string> PlayerSequence { get; set; }
    }
}
