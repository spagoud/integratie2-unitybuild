﻿using Assets.Scripts.Model.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Converters;

namespace Assets.Scripts.Model.Messages
{
    public class MessageBase
    {
        [JsonProperty("actor")]
        public string Actor { get; set; }

        [JsonProperty("actionType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public MessageType MessageType { get; set; }
    }
}
