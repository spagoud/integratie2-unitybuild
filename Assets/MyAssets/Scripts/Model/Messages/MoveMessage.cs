﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Messages
{
    public class MoveMessage:MessageBase
    {
        [JsonProperty("movement")] public List<Tile> Movement { get; set; }


    }
}
