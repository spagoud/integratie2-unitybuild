﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Messages
{
    public class AttackMessage:MessageBase
    {
        [JsonProperty("targedId")]
        public long TargetId { get; set; }

        [JsonProperty("damage")]
        public int Damage { get; set; }

        [JsonProperty("dead")]
        public bool Dead { get; set; }
    }
}
