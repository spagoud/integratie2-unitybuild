﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Messages
{
    public class ChestMessage : MessageBase
    {
        [JsonProperty("treasure")]
        public Item Treasure { get; set; }

        [JsonProperty("damage")]
        public int Damage { get; set; }

        [JsonProperty("tile")]
        public Tile Tile { get; set; }
    }
}
