﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model
{
    public class Room
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("tiles")]
        public Tile[] Tiles { get; set; }

        [JsonProperty("gameMonsters")]
        public List<GameMonster> GameMonsters { get; set; }

        [JsonProperty("doors")]
        public Door[] Doors { get; set; }

        [JsonProperty("starterRoom")]
        public bool StarterRoom { get; set; }
    }
}
