﻿using Newtonsoft.Json;

namespace Assets.Scripts.Model
{
    public class Hero
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("symbolImg")]
        public string SymbolImg { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("armour")]
        public int Armour { get; set; }

        [JsonProperty("move")]
        public int Move { get; set; }

        [JsonProperty("maxHealtPoints")]
        public int MaxHealthPoints { get; set; }

        [JsonProperty("maxMagicPoints")]
        public int MaxMagicPoints { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("invertorySpace")]
        public int InvertorySpace { get; set; }

        [JsonProperty("equipment")]
        public Equipment Equipment { get; set; }
    }
}