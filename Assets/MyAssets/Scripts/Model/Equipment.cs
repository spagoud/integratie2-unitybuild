﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model
{
    public class Equipment
    {
        [JsonProperty("weapon")]
        public Item Weapon { get; set; }
        [JsonProperty("artifact")]
        public Item Artifact { get; set; }
        [JsonProperty("extra")]
        public Item Extra { get; set; }
    }
}
