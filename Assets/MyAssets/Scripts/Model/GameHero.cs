﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model
{
    public class GameHero
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("userProfile")]
        public UserProfile UserProfile { get; set; }
        [JsonProperty("magicPoints")]
        public int MagicPoints { get; set; }
        [JsonProperty("healthPoints")]
        public int HealthPoints { get; set; }
        [JsonProperty("itemList")]
        public List<Item> ItemList { get; set; }
        [JsonProperty("equipment")]
        public Equipment Equipment { get; set; }
        [JsonProperty("maxMagicPoints")]
        public int MaxMagicPoints { get; set; }
        [JsonProperty("maxHealthPoints")]
        public int MaxHealthPoints { get; set; }
        [JsonProperty("heroName")]
        public String HeroName { get; set; }
        [JsonProperty("heroImage")]
        public String HeroImage { get; set; }
        [JsonProperty("inventorySpace")]
        public int InventorySpace { get; set; }
        [JsonProperty("type")]
        public String Type { get; set; }
        [JsonProperty("position")]
        public Tile Position { get; set; }
        [JsonProperty("move")]
        public int Move { get; set; }

    }
}
