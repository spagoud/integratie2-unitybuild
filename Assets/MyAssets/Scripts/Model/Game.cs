﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model
{
    public class Game
    {
        [JsonProperty("heroes")]
        public List<GameHero> Heroes { get; set; }
        [JsonProperty("storyDescription")]
        public String StoryDescription { get; set; }
        [JsonProperty("visibleRooms")]
        public List<Room> VisibleRooms { get; set; }
    }
}
