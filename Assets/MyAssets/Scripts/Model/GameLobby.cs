﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model
{
    public class GameLobby
    {
        [JsonProperty("lobbyName")]
        public string Name { get; set; }

        [JsonProperty("lobbyOwner")]
        public string Owner { get; set; }

        [JsonProperty("inviteOnly")]
        public string InviteOnly { get; set; }
    }
}
