﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model;
using Assets.Scripts.Model.Enums;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Scripts.Model.Actions.Sending
{
    public class DoorAction: Action
    {
        [JsonProperty("door")] private Door RoomDoor { get; set; }
  
        public DoorAction(String heroName, Door door, ActionType actionType) : base(heroName, actionType)
        {
            this.RoomDoor = door;
        }
    }
}
