﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model;
using Assets.Scripts.Model.Actions.Sending;
using Assets.Scripts.Model.Enums;
using Newtonsoft.Json;
using UnityEngine;

public class ChestAction : Action
{
    [JsonProperty("tile")] private Tile Tile { get; set; }

    public ChestAction(string heroName, ActionType actionType, Tile tile) : base(heroName, actionType)
    {
        Tile = tile;
    }
}
