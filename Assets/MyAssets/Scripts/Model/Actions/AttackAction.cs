﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model;
using Assets.Scripts.Model.Actions.Sending;
using Assets.Scripts.Model.Enums;
using Newtonsoft.Json;
using UnityEngine;

public class AttackAction : Action
{
    [JsonProperty("targetId")] private long TargetId { get; set; }
    [JsonProperty("itemId")] private long ItemId { get; set; }

    public AttackAction(string heroName, ActionType actionType, long targetId, long itemId) : base(heroName, actionType)
    {
        TargetId = targetId;
        ItemId = itemId;
    }
}
