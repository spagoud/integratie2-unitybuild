﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model;
using Assets.Scripts.Model.Enums;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Scripts.Model.Actions.Sending
{
    public class MoveAction: Action
    {
        [JsonProperty("movementPath")] private List<Tile> MovementPath { get; set; }
        
        public MoveAction(string heroName, List<Tile> movementPath, ActionType actionType):base(heroName, actionType)
        {
            MovementPath = movementPath;
        }
    }
}
