﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model.Enums;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Scripts.Model.Actions.Sending
{
    public class Action
    {
        [JsonProperty("heroName")] private string HeroName { get; set; }
        [JsonProperty("actionType")] private ActionType ActionType { get; set; }

        public Action(string heroName, ActionType actionType)
        {
            HeroName = heroName;
            ActionType = actionType;
        }
    }
}
