﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model
{
    public class Treasure
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("img")]
        public string Img { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("startersEquipment")]
        public bool StartersEquipment { get; set; }
    }
}
