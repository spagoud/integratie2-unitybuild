﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model
{
    public class GameMonster
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("symbolImg")]
        public String SymbolImg { get; set; }

        [JsonProperty("name")]
        public String Name { get; set; }

        [JsonProperty("description")]
        public String Description { get; set; }

        [JsonProperty("armor")]
        public int Armor { get; set; }

        [JsonProperty("move")]
        public int Move { get; set; }

        [JsonProperty("undeadScore")]
        public int UndeadScore { get; set; }

        [JsonProperty("meleeDices")]
        public List<Dice> MeleeDices { get; set; }

        [JsonProperty("rangedDices")]
        public List<Dice> RangedDices { get; set; }

        [JsonProperty("hitPoints")]
        public int HitPoints { get; set; }

        [JsonProperty("maxHitPoints")]
        public int MaxHitPoints { get; set; }

        [JsonProperty("position")]
        public Tile Position { get; set; }
    }
}
