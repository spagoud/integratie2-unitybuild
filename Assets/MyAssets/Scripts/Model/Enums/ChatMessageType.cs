﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Model.Enums
{
    public enum ChatMessageType
    {
        INFO,
        CHAT,
        JOIN,
        LEAVE
    }
}
