﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Enums
{
    public enum HeroType
    {
        HUMAN_KNIGHT, HUMAN_CLERIC, HALFLING_ROGUE, ELVEN_WIZARD
    }
}
