﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Enums
{
    public enum ActionType
    {
        MOVE,
        DOOR,
        TRADE,
        ATTACK,
        TRAP,
        CHEST,
        EQUIP,
        SWITCHITEM,
        POTION
    }
}