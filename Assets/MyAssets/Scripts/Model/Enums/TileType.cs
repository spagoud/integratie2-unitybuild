﻿namespace Assets.Scripts.Model.Enums

{
    public enum TileType
    {
        TOP, LEFT, RIGHT, BOTTOM,
        TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT, FLOOR
    }
}
