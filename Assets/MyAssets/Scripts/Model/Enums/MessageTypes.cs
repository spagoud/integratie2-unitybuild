﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Model.Enums
{
    public enum MessageType
    {
        MOVE,
        DOOR,
        CHEST,
        TRAP,
        ATTACK,
        TRADE,
        TURN,
        TURNSEQUENCE,
        POTION,
        EQUIP
    }
}
