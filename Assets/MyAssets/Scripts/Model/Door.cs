﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Assets.Scripts.Model
{
    public class Door
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("tile1")]
        public Tile Tile1 { get; set; }
        [JsonProperty("tile2")]
        public Tile Tile2 { get; set; }
        [JsonProperty("enabled")]
        public bool Enabled { get; set; }
        [JsonProperty("locked")]
        public bool Locked { get; set; }
    }
}
