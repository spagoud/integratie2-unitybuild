﻿using Newtonsoft.Json;

namespace Assets.Scripts.Model
{
    public class Item
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("img")]
        public string Img { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("starterEquipment")]
        public bool StartersEquipment { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}