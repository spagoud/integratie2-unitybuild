﻿using System.Collections.Generic;

namespace Assets.Scripts.Model
{
    public class Dice
    {
        public string Type { get; set; }
        public List<string> Faces { get; set; }
    }
}