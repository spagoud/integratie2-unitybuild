﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseResponse : MonoBehaviour, IPointerClickHandler
{
    private MovementController MovementController;
    private CameraController CameraController;

    void Start()
    {
        MovementController = GameObject.FindGameObjectWithTag("GameController").GetComponent<MovementController>();
        CameraController = GameObject.FindGameObjectWithTag("GameController").GetComponent<CameraController>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //CameraController.MoveCamera(transform.position);
        Debug.Log("tile: " + name);
        MovementController.AddTileToMovePath(gameObject);
        MovementController.SetMoveableArea();
    }
}
