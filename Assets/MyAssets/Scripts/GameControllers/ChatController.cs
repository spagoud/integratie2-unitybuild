﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.GameControllers;
using Assets.Scripts.Model.Enums;
using Assets.Scripts.Model.Messages;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public class ChatController : MonoBehaviour
{
    private string UserName = "Sam";
    public int MaxMessages = 25;

    public GameObject ChatPanel;
    public GameObject TextObject;
    public InputField ChatBox;

    public Color PlayerColor, InfoColor, UserColor;

    [SerializeField] List<ChatMessageObject> messageList = new List<ChatMessageObject>();

    public void SetUserName(string userName)
    {
        UserName = userName;
    }

    void Start()
    {
        AngularRecieverController.ChatEvent.AddListener(SendMessageToChat);
    }

    void Update()
    {
        if (ChatBox.text != "")
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                ChatMessage chatMessage = new ChatMessage()
                    {ChatMessageType = ChatMessageType.CHAT, Content = ChatBox.text, Sender = UserName};
                // ANGULAR COMMUNICATION
                AngularSenderController.GetInstance().ChatMessageRequest(chatMessage);

                // TESTING
                /*var chatMessageJson = "{\"sender\":\"admin\",\"chatMessageType\":1,\"content\":\"hey\"}";
                SendMessageToChat(chatMessage);*/

                ChatBox.text = "";
                ChatBox.ActivateInputField();
            }
        }
        else
        {
            if (!ChatBox.isFocused && Input.GetKeyDown(KeyCode.Return))
            {
                ChatBox.ActivateInputField();
            }
        }
    }

    public void SendMessageToChat(ChatMessage chatMessage)
    {
        if (messageList.Count >= MaxMessages)
        {
            Destroy(messageList[0].textObject.gameObject);
            messageList.Remove(messageList[0]);
        }

        ChatMessageObject newMessage = new ChatMessageObject();

        GameObject newText = Instantiate(TextObject, ChatPanel.transform);
        newMessage.textObject = newText.GetComponent<Text>();
        newMessage.textObject.text = chatMessage.Sender + ": " + chatMessage.Content;
        newMessage.textObject.color = chatMessage.Sender.Equals(UserName) ? UserColor : ChatMessageTypeColor(chatMessage.ChatMessageType);
        newMessage.ChatMessage = chatMessage;

        messageList.Add(newMessage);
    }

    Color ChatMessageTypeColor(ChatMessageType chatMessageType)
    {
        Color color = InfoColor;
        switch (chatMessageType)
        {
            case ChatMessageType.CHAT:
                color = PlayerColor;
                break;
        }

        return color;
    }
}

[Serializable]
public class ChatMessageObject
{
    public Text textObject;
    public ChatMessage ChatMessage;
}