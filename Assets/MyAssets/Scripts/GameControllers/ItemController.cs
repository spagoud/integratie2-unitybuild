﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameControllers;
using Assets.Scripts.Model;
using UnityEngine;
using UnityEngine.UI;

public class ItemController : MonoBehaviour
{
    private GameController GameController;
    private AttackController AttackController;

    public GameObject Weapon;
    public GameObject Artifact;
    public GameObject Spell;

    public List<GameObject> Treasures;
    public List<Button> Buttons = new List<Button>();
    public GameObject FifthTreasureKnapsack;

    public List<IconObjectMap> IconObjectMaps;

    void Start()
    {
        GameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        AttackController = GameObject.FindGameObjectWithTag("GameController").GetComponent<AttackController>();
        Buttons = new List<Button>();
        foreach (var treasure in Treasures)
        {
            treasure.transform.Find("OverLay").Find("Drop").GetComponent<Button>().interactable = false;
            Buttons.Add(treasure.transform.Find("OverLay").Find("Equip").GetComponent<Button>());
        }
    }

    public void EnableTreasureButtons(bool enableButtons)
    {
        foreach (Button button in Buttons)
        {
            button.interactable = enableButtons;
        }
    }

    public void LoadInStarterEquipment(Equipment startEquipment, int inventorySpace)
    {
        //TODO: Display Damage
        if (startEquipment.Weapon != null)
        {
            DisplayItemInUI(Weapon, startEquipment.Weapon);
        }

        if (startEquipment.Artifact != null)
        {
            DisplayItemInUI(Artifact, startEquipment.Artifact);
        }

        if (startEquipment.Extra != null)
        {
            DisplayItemInUI(Spell, startEquipment.Extra);
        }

        if (inventorySpace < 5)
        {
            FifthTreasureKnapsack.SetActive(false);
        }
    }

    public void SelectEquipmentAndAttack(int number)
    {
        switch (number)
        {
            case 1:
                AttackController.AttackMonsterSetup(Weapon.GetComponent<ItemModel>().Item.Id);
                break;
            case 2:
                AttackController.AttackMonsterSetup(Artifact.GetComponent<ItemModel>().Item.Id);
                break;
            case 3:
                AttackController.AttackMonsterSetup(Spell.GetComponent<ItemModel>().Item.Id);
                break;
        }
    }

    public void AddTreasureToKnapsack(Item treasure)
    {
        GameObject treasureSlot = Treasures.First(go=>go.activeSelf == false);
        DisplayItemInUI(treasureSlot, treasure);
    }

    public void DisplayItemInUI(GameObject itemSlot, Item treasure)
    {
        itemSlot.SetActive(true);
        itemSlot.transform.Find("Image").GetComponent<Image>().sprite =
            IconObjectMaps.Find(iom => iom.ImageName.Equals(treasure.Type)).Sprite;
        itemSlot.transform.Find("Name").GetComponent<Text>().text = treasure.Name;
        itemSlot.GetComponent<ItemModel>().Item = treasure;
    }
}

[Serializable]
public class IconObjectMap
{
    public string ImageName;
    public Sprite Sprite;
}