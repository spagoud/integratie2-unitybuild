﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model;
using Assets.Scripts.Model.Actions.Sending;
using Assets.Scripts.Model.Enums;
using Assets.Scripts.Model.Messages;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Scripts.GameControllers
{
    [RequireComponent(typeof(DungeonController))]
    [RequireComponent(typeof(HeroController))]
    [RequireComponent(typeof(MonsterController))]
    [RequireComponent(typeof(UiController))]
    [RequireComponent(typeof(MovementController))]
    [RequireComponent(typeof(CameraController))]
    [RequireComponent(typeof(ChatController))]
    [RequireComponent(typeof(ItemController))]
    public class GameController : MonoBehaviour
    {
        public DungeonController DungeonController { get; set; }
        public HeroController HeroController { get; set; }
        public MonsterController MonsterController { get; set; }
        public UiController UiController { get; set; }
        public MovementController MovementController { get; set; }
        public CameraController CameraController { get; set; }
        public ChatController ChatController { get; set; }
        public ItemController ItemController { get; set; }

        public AngularSenderController angularSenderController;

        private string User;
        private string UserHero;
        public GameObject UserGameHero { get; set; }
        private string CurrentHero;
        private MessageBase Turn;
        private SequenceMessage Sequence;

        private int NumberOfActions;
        private bool Spectator;

        private void Start()
        {
            DungeonController = GetComponent<DungeonController>();
            HeroController = GetComponent<HeroController>();
            MonsterController = GetComponent<MonsterController>();
            UiController = GetComponent<UiController>();
            MovementController = GetComponent<MovementController>();
            CameraController = GetComponent<CameraController>();
            ChatController = GetComponent<ChatController>();
            ItemController = GetComponent<ItemController>();

            // ANGULAR COMMUNICATION
            // initial events
            AngularRecieverController.SetUserEvent.AddListener(SetUser);
            AngularRecieverController.Turnevent.AddListener(SetTurn);
            AngularRecieverController.SequenceEvent.AddListener(SetSequence);

            AngularRecieverController.GameStartedEvent.AddListener(setGame);
            AngularSenderController.GetInstance().StartGameRequest();
            AngularRecieverController.RangeEvent.AddListener(SetRange);
            AngularRecieverController.MoveEvent.AddListener(MoveHeroResponse);
            AngularRecieverController.OpenDoorEvent.AddListener(OpenDoorResponse);
            AngularRecieverController.TrapEvent.AddListener(TrapResponse);
            AngularRecieverController.ChestEvent.AddListener(OpenChestResponse);
            AngularRecieverController.AttackEvent.AddListener(AttackResponse);
            AngularRecieverController.EndGameEvent.AddListener(EndGameResponse);


            // TESTING
            /*SetUser("admin");
            var turnSequenceJson =
                "{\"actor\":null,\"actionType\":\"TURNSEQUENCE\",\"playerSequence\":[\"JozanAI\",\"LiddaAI\",\"Regdar\",\"MialeeAI\"]}";
            SetSequence(JsonConvert.DeserializeObject<SequenceMessage>(turnSequenceJson));
            var turnJson = "{\"actor\":\"Regdar\",\"actionType\":\"TURN\"}";
            SetTurn(JsonConvert.DeserializeObject<MessageBase>(turnJson));

            //var JsonEntireDungeon =
            //    "{\"heroes\":[{\"userProfile\":{\"username\":\"admin\",\"profilePicture\":null},\"magicPoints\":0,\"healthPoints\":8,\"itemList\":[],\"equipment\":{\"weapon\":{\"name\":\"Single-handed broadsword\",\"description\":\"Description\",\"img\":\"image\",\"level\":1,\"startersEquipment\":false,\"type\":\"Weapon\"},\"artifact\":null,\"extra\":null},\"maxMagicPoints\":0,\"maxHealthPoints\":8,\"heroName\":\"Regdar\",\"heroImage\":\"KnightImage\",\"inventorySpace\":4,\"type\":\"Human Knight\",\"position\":{\"id\":516,\"tileType\":\"FLOOR\",\"x\":8,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},\"move\":4}],\"storyDescription\":\"Unease and darkness have fallen over the land of Rallion as Monsters ravage the region. Travelling through it, the Heroes have arrived at the village of Holbrook, on the edge of a forest, where Goblin attacks have left the villagers fearing for their lives. The Sheriff of Holbrook has gone in search of them, but has not returned. The Goblins must be the key to his disappearance.\",\"visibleRooms\":[{\"name\":\"D1R1\",\"tiles\":[{\"id\":511,\"tileType\":\"TOPLEFT\",\"x\":7,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":512,\"tileType\":\"TOP\",\"x\":8,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":513,\"tileType\":\"TOP\",\"x\":9,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":514,\"tileType\":\"TOPRIGHT\",\"x\":10,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":515,\"tileType\":\"LEFT\",\"x\":7,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":516,\"tileType\":\"FLOOR\",\"x\":8,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":517,\"tileType\":\"FLOOR\",\"x\":9,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":518,\"tileType\":\"RIGHT\",\"x\":10,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":519,\"tileType\":\"LEFT\",\"x\":7,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":520,\"tileType\":\"FLOOR\",\"x\":8,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":521,\"tileType\":\"FLOOR\",\"x\":9,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":522,\"tileType\":\"RIGHT\",\"x\":10,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":523,\"tileType\":\"TOPLEFT\",\"x\":4,\"y\":3,\"pillar\":true,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":524,\"tileType\":\"TOP\",\"x\":5,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":525,\"tileType\":\"TOP\",\"x\":6,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":526,\"tileType\":\"FLOOR\",\"x\":7,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":527,\"tileType\":\"FLOOR\",\"x\":8,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":528,\"tileType\":\"FLOOR\",\"x\":9,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"PIT\",\"door\":false},{\"id\":529,\"tileType\":\"RIGHT\",\"x\":10,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":530,\"tileType\":\"LEFT\",\"x\":4,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":531,\"tileType\":\"FLOOR\",\"x\":5,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":532,\"tileType\":\"FLOOR\",\"x\":6,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":533,\"tileType\":\"FLOOR\",\"x\":7,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"PIT\",\"door\":false},{\"id\":534,\"tileType\":\"FLOOR\",\"x\":8,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":535,\"tileType\":\"FLOOR\",\"x\":9,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":536,\"tileType\":\"RIGHT\",\"x\":10,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":537,\"tileType\":\"LEFT\",\"x\":4,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":538,\"tileType\":\"FLOOR\",\"x\":5,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":539,\"tileType\":\"FLOOR\",\"x\":6,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":540,\"tileType\":\"FLOOR\",\"x\":7,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":541,\"tileType\":\"FLOOR\",\"x\":8,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":542,\"tileType\":\"FLOOR\",\"x\":9,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"PIT\",\"door\":false},{\"id\":543,\"tileType\":\"RIGHT\",\"x\":10,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":544,\"tileType\":\"BOTTOMLEFT\",\"x\":4,\"y\":6,\"pillar\":true,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":545,\"tileType\":\"BOTTOM\",\"x\":5,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":546,\"tileType\":\"BOTTOM\",\"x\":6,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":547,\"tileType\":\"BOTTOM\",\"x\":7,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":548,\"tileType\":\"BOTTOM\",\"x\":8,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":549,\"tileType\":\"BOTTOM\",\"x\":9,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":550,\"tileType\":\"BOTTOMRIGHT\",\"x\":10,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}],\"gameMonsters\":[],\"doors\":[{\"name\":\"R1R2\",\"tile1\":{\"id\":529,\"tileType\":\"RIGHT\",\"x\":10,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":569,\"tileType\":\"LEFT\",\"x\":11,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false},{\"name\":\"R1R4\",\"tile1\":{\"id\":547,\"tileType\":\"BOTTOM\",\"x\":7,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":629,\"tileType\":\"TOP\",\"x\":7,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false}],\"starterRoom\":false},{\"name\":\"D1R2\",\"tiles\":[{\"id\":551,\"tileType\":\"TOPLEFT\",\"x\":11,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":552,\"tileType\":\"TOP\",\"x\":12,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":553,\"tileType\":\"TOP\",\"x\":13,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":554,\"tileType\":\"TOP\",\"x\":14,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":555,\"tileType\":\"TOP\",\"x\":15,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":556,\"tileType\":\"TOPRIGHT\",\"x\":16,\"y\":0,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":557,\"tileType\":\"LEFT\",\"x\":11,\"y\":1,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":558,\"tileType\":\"FLOOR\",\"x\":12,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":559,\"tileType\":\"FLOOR\",\"x\":13,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":560,\"tileType\":\"FLOOR\",\"x\":14,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":561,\"tileType\":\"FLOOR\",\"x\":15,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":562,\"tileType\":\"RIGHT\",\"x\":16,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":563,\"tileType\":\"LEFT\",\"x\":11,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":564,\"tileType\":\"FLOOR\",\"x\":12,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":565,\"tileType\":\"FLOOR\",\"x\":13,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":566,\"tileType\":\"FLOOR\",\"x\":14,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":567,\"tileType\":\"FLOOR\",\"x\":15,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":568,\"tileType\":\"RIGHT\",\"x\":16,\"y\":2,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":569,\"tileType\":\"LEFT\",\"x\":11,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":570,\"tileType\":\"FLOOR\",\"x\":12,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":571,\"tileType\":\"FLOOR\",\"x\":13,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":572,\"tileType\":\"FLOOR\",\"x\":14,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":573,\"tileType\":\"FLOOR\",\"x\":15,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":574,\"tileType\":\"RIGHT\",\"x\":16,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":575,\"tileType\":\"LEFT\",\"x\":11,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":576,\"tileType\":\"FLOOR\",\"x\":12,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":577,\"tileType\":\"FLOOR\",\"x\":13,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":578,\"tileType\":\"FLOOR\",\"x\":14,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":579,\"tileType\":\"FLOOR\",\"x\":15,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":580,\"tileType\":\"RIGHT\",\"x\":16,\"y\":4,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":581,\"tileType\":\"LEFT\",\"x\":11,\"y\":5,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":582,\"tileType\":\"FLOOR\",\"x\":12,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":583,\"tileType\":\"FLOOR\",\"x\":13,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":584,\"tileType\":\"FLOOR\",\"x\":14,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":585,\"tileType\":\"FLOOR\",\"x\":15,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":586,\"tileType\":\"RIGHT\",\"x\":16,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":587,\"tileType\":\"BOTTOMLEFT\",\"x\":11,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":588,\"tileType\":\"BOTTOM\",\"x\":12,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":589,\"tileType\":\"BOTTOM\",\"x\":13,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":590,\"tileType\":\"BOTTOM\",\"x\":14,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":591,\"tileType\":\"BOTTOM\",\"x\":15,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":592,\"tileType\":\"BOTTOMRIGHT\",\"x\":16,\"y\":6,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false}],\"gameMonsters\":[],\"doors\":[{\"name\":\"R1R2\",\"tile1\":{\"id\":529,\"tileType\":\"RIGHT\",\"x\":10,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":569,\"tileType\":\"LEFT\",\"x\":11,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false}],\"starterRoom\":false},{\"name\":\"D1R3\",\"tiles\":[{\"id\":593,\"tileType\":\"TOPLEFT\",\"x\":17,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":594,\"tileType\":\"TOP\",\"x\":18,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":595,\"tileType\":\"TOP\",\"x\":19,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":596,\"tileType\":\"TOP\",\"x\":20,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":597,\"tileType\":\"TOPRIGHT\",\"x\":21,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":598,\"tileType\":\"LEFT\",\"x\":17,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":599,\"tileType\":\"FLOOR\",\"x\":18,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":600,\"tileType\":\"FLOOR\",\"x\":19,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":601,\"tileType\":\"FLOOR\",\"x\":20,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":602,\"tileType\":\"RIGHT\",\"x\":21,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":603,\"tileType\":\"LEFT\",\"x\":17,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":604,\"tileType\":\"FLOOR\",\"x\":18,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":605,\"tileType\":\"FLOOR\",\"x\":19,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":606,\"tileType\":\"FLOOR\",\"x\":20,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":607,\"tileType\":\"RIGHT\",\"x\":21,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":608,\"tileType\":\"LEFT\",\"x\":17,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":609,\"tileType\":\"FLOOR\",\"x\":18,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":610,\"tileType\":\"FLOOR\",\"x\":19,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":611,\"tileType\":\"FLOOR\",\"x\":20,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":612,\"tileType\":\"RIGHT\",\"x\":21,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":613,\"tileType\":\"BOTTOMLEFT\",\"x\":17,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":614,\"tileType\":\"BOTTOM\",\"x\":18,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":615,\"tileType\":\"BOTTOM\",\"x\":19,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":616,\"tileType\":\"BOTTOM\",\"x\":20,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":617,\"tileType\":\"BOTTOMRIGHT\",\"x\":21,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}],\"gameMonsters\":[],\"doors\":[{\"name\":\"R3R6\",\"tile1\":{\"id\":616,\"tileType\":\"BOTTOM\",\"x\":20,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":693,\"tileType\":\"TOP\",\"x\":20,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false}],\"starterRoom\":true},{\"name\":\"D1R4\",\"tiles\":[{\"id\":618,\"tileType\":\"TOPLEFT\",\"x\":0,\"y\":6,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":619,\"tileType\":\"TOP\",\"x\":1,\"y\":6,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":620,\"tileType\":\"TOP\",\"x\":2,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":621,\"tileType\":\"TOPRIGHT\",\"x\":3,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":622,\"tileType\":\"LEFT\",\"x\":0,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":623,\"tileType\":\"FLOOR\",\"x\":1,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":624,\"tileType\":\"FLOOR\",\"x\":2,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":625,\"tileType\":\"FLOOR\",\"x\":3,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":626,\"tileType\":\"TOP\",\"x\":4,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":627,\"tileType\":\"TOP\",\"x\":5,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":628,\"tileType\":\"TOP\",\"x\":6,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":629,\"tileType\":\"TOP\",\"x\":7,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":630,\"tileType\":\"TOP\",\"x\":8,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":631,\"tileType\":\"TOP\",\"x\":9,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":632,\"tileType\":\"TOPRIGHT\",\"x\":10,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":633,\"tileType\":\"LEFT\",\"x\":0,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":634,\"tileType\":\"FLOOR\",\"x\":1,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":635,\"tileType\":\"FLOOR\",\"x\":2,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":636,\"tileType\":\"FLOOR\",\"x\":3,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":637,\"tileType\":\"FLOOR\",\"x\":4,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":638,\"tileType\":\"FLOOR\",\"x\":5,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":639,\"tileType\":\"FLOOR\",\"x\":6,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":640,\"tileType\":\"FLOOR\",\"x\":7,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":641,\"tileType\":\"FLOOR\",\"x\":8,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":642,\"tileType\":\"FLOOR\",\"x\":9,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":643,\"tileType\":\"RIGHT\",\"x\":10,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":644,\"tileType\":\"LEFT\",\"x\":0,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":645,\"tileType\":\"FLOOR\",\"x\":1,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":646,\"tileType\":\"FLOOR\",\"x\":2,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":647,\"tileType\":\"FLOOR\",\"x\":3,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":648,\"tileType\":\"FLOOR\",\"x\":4,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":649,\"tileType\":\"FLOOR\",\"x\":5,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":650,\"tileType\":\"FLOOR\",\"x\":6,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":651,\"tileType\":\"FLOOR\",\"x\":7,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":652,\"tileType\":\"FLOOR\",\"x\":8,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":653,\"tileType\":\"FLOOR\",\"x\":9,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":654,\"tileType\":\"RIGHT\",\"x\":10,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":655,\"tileType\":\"BOTTOMLEFT\",\"x\":0,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":656,\"tileType\":\"BOTTOM\",\"x\":1,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":657,\"tileType\":\"BOTTOM\",\"x\":2,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":658,\"tileType\":\"BOTTOM\",\"x\":3,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":659,\"tileType\":\"BOTTOM\",\"x\":4,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":660,\"tileType\":\"BOTTOM\",\"x\":5,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":661,\"tileType\":\"BOTTOM\",\"x\":6,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":662,\"tileType\":\"BOTTOM\",\"x\":7,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":663,\"tileType\":\"BOTTOM\",\"x\":8,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":664,\"tileType\":\"BOTTOM\",\"x\":9,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":665,\"tileType\":\"BOTTOMRIGHT\",\"x\":10,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}],\"gameMonsters\":[],\"doors\":[{\"name\":\"R1R4\",\"tile1\":{\"id\":547,\"tileType\":\"BOTTOM\",\"x\":7,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":629,\"tileType\":\"TOP\",\"x\":7,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false},{\"name\":\"R4R5\",\"tile1\":{\"id\":643,\"tileType\":\"RIGHT\",\"x\":10,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":672,\"tileType\":\"LEFT\",\"x\":11,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false}],\"starterRoom\":false},{\"name\":\"D1R5\",\"tiles\":[{\"id\":666,\"tileType\":\"TOPLEFT\",\"x\":11,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":667,\"tileType\":\"TOP\",\"x\":12,\"y\":7,\"pillar\":true,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":668,\"tileType\":\"TOP\",\"x\":13,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":669,\"tileType\":\"TOP\",\"x\":14,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":670,\"tileType\":\"TOP\",\"x\":15,\"y\":7,\"pillar\":true,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":671,\"tileType\":\"TOPRIGHT\",\"x\":16,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":672,\"tileType\":\"LEFT\",\"x\":11,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":673,\"tileType\":\"FLOOR\",\"x\":12,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":674,\"tileType\":\"FLOOR\",\"x\":13,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":675,\"tileType\":\"FLOOR\",\"x\":14,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":676,\"tileType\":\"FLOOR\",\"x\":15,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":677,\"tileType\":\"RIGHT\",\"x\":16,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":678,\"tileType\":\"LEFT\",\"x\":11,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":679,\"tileType\":\"FLOOR\",\"x\":12,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":680,\"tileType\":\"FLOOR\",\"x\":13,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":681,\"tileType\":\"FLOOR\",\"x\":14,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":682,\"tileType\":\"FLOOR\",\"x\":15,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":683,\"tileType\":\"RIGHT\",\"x\":16,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":684,\"tileType\":\"BOTTOMLEFT\",\"x\":11,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":685,\"tileType\":\"BOTTOM\",\"x\":12,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":686,\"tileType\":\"BOTTOM\",\"x\":13,\"y\":10,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":687,\"tileType\":\"BOTTOM\",\"x\":14,\"y\":10,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":688,\"tileType\":\"BOTTOM\",\"x\":15,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":689,\"tileType\":\"BOTTOMRIGHT\",\"x\":16,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}],\"gameMonsters\":[],\"doors\":[{\"name\":\"R4R5\",\"tile1\":{\"id\":643,\"tileType\":\"RIGHT\",\"x\":10,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":672,\"tileType\":\"LEFT\",\"x\":11,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false},{\"name\":\"R5R6\",\"tile1\":{\"id\":677,\"tileType\":\"RIGHT\",\"x\":16,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":705,\"tileType\":\"LEFT\",\"x\":17,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false}],\"starterRoom\":false},{\"name\":\"D1R6\",\"tiles\":[{\"id\":690,\"tileType\":\"TOPLEFT\",\"x\":17,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":691,\"tileType\":\"TOP\",\"x\":18,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":692,\"tileType\":\"TOP\",\"x\":19,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":693,\"tileType\":\"TOP\",\"x\":20,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":694,\"tileType\":\"TOPRIGHT\",\"x\":21,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":695,\"tileType\":\"LEFT\",\"x\":17,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":696,\"tileType\":\"FLOOR\",\"x\":18,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":697,\"tileType\":\"FLOOR\",\"x\":19,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":698,\"tileType\":\"FLOOR\",\"x\":20,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":699,\"tileType\":\"RIGHT\",\"x\":21,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":700,\"tileType\":\"LEFT\",\"x\":17,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":701,\"tileType\":\"FLOOR\",\"x\":18,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":702,\"tileType\":\"FLOOR\",\"x\":19,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":703,\"tileType\":\"FLOOR\",\"x\":20,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":704,\"tileType\":\"RIGHT\",\"x\":21,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":705,\"tileType\":\"LEFT\",\"x\":17,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":706,\"tileType\":\"FLOOR\",\"x\":18,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":707,\"tileType\":\"FLOOR\",\"x\":19,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":708,\"tileType\":\"FLOOR\",\"x\":20,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":709,\"tileType\":\"RIGHT\",\"x\":21,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":710,\"tileType\":\"LEFT\",\"x\":17,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":711,\"tileType\":\"FLOOR\",\"x\":18,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":712,\"tileType\":\"FLOOR\",\"x\":19,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":713,\"tileType\":\"FLOOR\",\"x\":20,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":714,\"tileType\":\"RIGHT\",\"x\":21,\"y\":9,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":715,\"tileType\":\"BOTTOMLEFT\",\"x\":17,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":716,\"tileType\":\"BOTTOM\",\"x\":18,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":717,\"tileType\":\"BOTTOM\",\"x\":19,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":718,\"tileType\":\"BOTTOM\",\"x\":20,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":719,\"tileType\":\"BOTTOMRIGHT\",\"x\":21,\"y\":10,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}],\"gameMonsters\":[],\"doors\":[{\"name\":\"R3R6\",\"tile1\":{\"id\":616,\"tileType\":\"BOTTOM\",\"x\":20,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":693,\"tileType\":\"TOP\",\"x\":20,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false},{\"name\":\"R5R6\",\"tile1\":{\"id\":677,\"tileType\":\"RIGHT\",\"x\":16,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":705,\"tileType\":\"LEFT\",\"x\":17,\"y\":8,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false}],\"starterRoom\":false}]}";
            var room1 = "{\"heroes\":[{\"userProfile\":{\"username\":\"admin\",\"profilePicture\":null},\"magicPoints\":0,\"healthPoints\":8,\"itemList\":[],\"equipment\":{\"weapon\":{\"id\":23,\"name\":\"Single-handed broadsword\",\"description\":\"Description\",\"img\":\"image\",\"level\":1,\"startersEquipment\":false,\"type\":\"Weapon\"},\"artifact\":null,\"extra\":null},\"maxMagicPoints\":0,\"maxHealthPoints\":8,\"heroName\":\"Regdar\",\"heroImage\":\"KnightImage\",\"inventorySpace\":4,\"type\":\"Human Knight\",\"position\":{\"id\":545,\"tileType\":\"RIGHT\",\"x\":10,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"move\":4},{\"userProfile\":null,\"magicPoints\":5,\"healthPoints\":5,\"itemList\":[],\"equipment\":{\"weapon\":{\"id\":22,\"name\":\"Short bow of the ancients\",\"description\":\"Description\",\"img\":\"image\",\"level\":1,\"startersEquipment\":false,\"type\":\"Weapon\"},\"artifact\":null,\"extra\":{\"id\":31,\"name\":\"Magic missile\",\"description\":\"Description\",\"img\":\"image\",\"level\":1,\"startersEquipment\":false,\"type\":\"Spell\"}},\"maxMagicPoints\":5,\"maxHealthPoints\":5,\"heroName\":\"MialeeAI\",\"heroImage\":\"MageImage\",\"inventorySpace\":5,\"type\":\"Elven Wizard\",\"position\":{\"id\":533,\"tileType\":\"FLOOR\",\"x\":9,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},\"move\":5},{\"userProfile\":null,\"magicPoints\":5,\"healthPoints\":5,\"itemList\":[],\"equipment\":{\"weapon\":{\"id\":14,\"name\":\"Crossbow of faith\",\"description\":\"Description\",\"img\":\"image\",\"level\":1,\"startersEquipment\":false,\"type\":\"Weapon\"},\"artifact\":null,\"extra\":{\"id\":29,\"name\":\"Greater restoration\",\"description\":\"Description\",\"img\":\"image\",\"level\":1,\"startersEquipment\":false,\"type\":\"Spell\"}},\"maxMagicPoints\":5,\"maxHealthPoints\":5,\"heroName\":\"JozanAI\",\"heroImage\":\"ClericImage\",\"inventorySpace\":5,\"type\":\"Human Cleric\",\"position\":{\"id\":536,\"tileType\":\"FLOOR\",\"x\":8,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},\"move\":5},{\"userProfile\":null,\"magicPoints\":0,\"healthPoints\":5,\"itemList\":[],\"equipment\":{\"weapon\":{\"id\":9,\"name\":\"Balanced Throwing Dagger\",\"description\":\"Description\",\"img\":\"image\",\"level\":1,\"startersEquipment\":false,\"type\":\"Weapon\"},\"artifact\":{\"id\":39,\"name\":\"Yondalla's amulet\",\"description\":\"Description\",\"img\":\"image\",\"level\":1,\"startersEquipment\":false,\"type\":\"Artifact\"},\"extra\":null},\"maxMagicPoints\":0,\"maxHealthPoints\":5,\"heroName\":\"LiddaAI\",\"heroImage\":\"RogueImage\",\"inventorySpace\":4,\"type\":\"Halfling Rogue\",\"position\":{\"id\":537,\"tileType\":\"FLOOR\",\"x\":9,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},\"move\":6}],\"storyDescription\":\"Unease and darkness have fallen over the land of Rallion as Monsters ravage the region. Travelling through it, the Heroes have arrived at the village of Holbrook, on the edge of a forest, where Goblin attacks have left the villagers fearing for their lives. The Sheriff of Holbrook has gone in search of them, but has not returned. The Goblins must be the key to his disappearance.\",\"visibleRooms\":[{\"name\":\"D1R1\",\"tiles\":[{\"id\":527,\"tileType\":\"TOPLEFT\",\"x\":7,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":528,\"tileType\":\"TOP\",\"x\":8,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":529,\"tileType\":\"TOP\",\"x\":9,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":530,\"tileType\":\"TOPRIGHT\",\"x\":10,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":531,\"tileType\":\"LEFT\",\"x\":7,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":532,\"tileType\":\"FLOOR\",\"x\":8,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":533,\"tileType\":\"FLOOR\",\"x\":9,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":534,\"tileType\":\"RIGHT\",\"x\":10,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":535,\"tileType\":\"LEFT\",\"x\":7,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":536,\"tileType\":\"FLOOR\",\"x\":8,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":537,\"tileType\":\"FLOOR\",\"x\":9,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":538,\"tileType\":\"RIGHT\",\"x\":10,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":539,\"tileType\":\"TOPLEFT\",\"x\":4,\"y\":3,\"pillar\":true,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":540,\"tileType\":\"TOP\",\"x\":5,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":541,\"tileType\":\"TOP\",\"x\":6,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":542,\"tileType\":\"FLOOR\",\"x\":7,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":543,\"tileType\":\"FLOOR\",\"x\":8,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":544,\"tileType\":\"FLOOR\",\"x\":9,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"PIT\",\"door\":false},{\"id\":545,\"tileType\":\"RIGHT\",\"x\":10,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":546,\"tileType\":\"LEFT\",\"x\":4,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":547,\"tileType\":\"FLOOR\",\"x\":5,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":548,\"tileType\":\"FLOOR\",\"x\":6,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":549,\"tileType\":\"FLOOR\",\"x\":7,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"PIT\",\"door\":false},{\"id\":550,\"tileType\":\"FLOOR\",\"x\":8,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":551,\"tileType\":\"FLOOR\",\"x\":9,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":552,\"tileType\":\"RIGHT\",\"x\":10,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":553,\"tileType\":\"LEFT\",\"x\":4,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":554,\"tileType\":\"FLOOR\",\"x\":5,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":555,\"tileType\":\"FLOOR\",\"x\":6,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":556,\"tileType\":\"FLOOR\",\"x\":7,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":557,\"tileType\":\"FLOOR\",\"x\":8,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":558,\"tileType\":\"FLOOR\",\"x\":9,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"PIT\",\"door\":false},{\"id\":559,\"tileType\":\"RIGHT\",\"x\":10,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":560,\"tileType\":\"BOTTOMLEFT\",\"x\":4,\"y\":6,\"pillar\":true,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":561,\"tileType\":\"BOTTOM\",\"x\":5,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":562,\"tileType\":\"BOTTOM\",\"x\":6,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":563,\"tileType\":\"BOTTOM\",\"x\":7,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":564,\"tileType\":\"BOTTOM\",\"x\":8,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":565,\"tileType\":\"BOTTOM\",\"x\":9,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":566,\"tileType\":\"BOTTOMRIGHT\",\"x\":10,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}],\"gameMonsters\":null,\"doors\":[{\"id\":736,\"name\":\"R1R2\",\"tile1\":{\"id\":545,\"tileType\":\"RIGHT\",\"x\":10,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":585,\"tileType\":\"LEFT\",\"x\":11,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false},{\"id\":737,\"name\":\"R1R4\",\"tile1\":{\"id\":563,\"tileType\":\"BOTTOM\",\"x\":7,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":645,\"tileType\":\"TOP\",\"x\":7,\"y\":7,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":false,\"locked\":false}],\"starterRoom\":false}]}";
            setGame(JsonConvert.DeserializeObject<Game>(room1));*/
        }

        public bool IsTurn()
        {
            if(UserHero.Equals(CurrentHero))
            {
                return true;
            }

            return false;
        }

        private void SetUser(string userName)
        {
            User = userName;
            ChatController.SetUserName(User);
        }

        private void SetTurn(MessageBase messageBase)
        {
            CurrentHero = messageBase.Actor;
            Turn = messageBase;
            AngularRecieverController.Ready = true;
        }

        private void SetSequence(SequenceMessage sequenceMessage)
        {
            Sequence = sequenceMessage;
            AngularRecieverController.Ready = true;
        }

        public void UpdateUiActionsLeftAndButtons(string name)
        {
            if (name.Equals(UserHero) && CurrentHero.Equals(UserHero))
            {
                UiController.SetActionsText("Actions left: " + --NumberOfActions);
                UiController.EnableActionButtons(true,
                    HeroController.GetHeroGameObjectByName(UserHero).GetComponent<CharacterModel>().TileModel.Tile);
            }
        }

        private void setGame(Game game)
        {
            foreach (Room gameVisibleRoom in game.VisibleRooms)
            {
                DungeonController.AddRoom(gameVisibleRoom);
            }

            HeroController.GenerateHeroes(game.Heroes);
            SetCurrentHero();

            if (Turn != null)
            {
                TurnHandler(Turn);
            }

            AngularRecieverController.Turnevent.RemoveAllListeners();
            AngularRecieverController.Turnevent.AddListener(TurnHandler);

            UiController.DisableLoadingScreen();

            if (Sequence != null)
            {
                DisplaySequence(Sequence);
            }

            AngularRecieverController.SequenceEvent.RemoveAllListeners();
            AngularRecieverController.SequenceEvent.AddListener(DisplaySequence);
            UiController.SetStoryDescription(game.StoryDescription);
        }

        private void SetCurrentHero()
        {
            foreach (GameObject gameHero in HeroController.GameHeroes)
            {
                if (gameHero.GetComponent<CharacterModel>().GameHero.UserProfile != null)
                {
                    if (gameHero.GetComponent<CharacterModel>().GameHero.UserProfile.Username.Equals(User))
                    {
                        UserHero = gameHero.name;
                    }
                }
            }

            if (UserHero != null)
            {
                Debug.Log("Current hero: " + UserHero);
                UserGameHero = HeroController.GetHeroGameObjectByName(UserHero);
                UiController.SetHeroNameText(UserHero);
                UiController.UpdateHeroStats(HeroController.GetHeroGameObjectByName(UserHero)
                    .GetComponent<CharacterModel>().GameHero);
                CameraController.SetMainGameHero(HeroController.GetHeroGameObjectByName(UserHero).transform);
                ItemController.LoadInStarterEquipment(HeroController.GetHeroGameObjectByName(UserHero)
                    .GetComponent<CharacterModel>().GameHero.Equipment, HeroController.GetHeroGameObjectByName(UserHero)
                    .GetComponent<CharacterModel>().GameHero.InventorySpace);
            }
            else
            {
                Debug.Log("No current hero");
                Spectator = true;
                UiController.DisplayMainMenu(false);
                UiController.EnableSpectatorDisplay(true);
                SpectatorWatchHero("Regdar");
            }
        }

        private void DisplaySequence(SequenceMessage sequence)
        {
            UiController.DisplaySequence(sequence.PlayerSequence);
            AngularRecieverController.Ready = true;
        }

        private void TurnHandler(MessageBase turnBase)
        {
            CurrentHero = turnBase.Actor;
            if (!Spectator)
            {
                if (turnBase.Actor.Equals(UserHero))
                {
                    NumberOfActions = 2;
                    UiController.EnableActionButtons(true,
                        HeroController.GetHeroGameObjectByName(UserHero).GetComponent<CharacterModel>().TileModel.Tile);
                    UiController.SetTurnText("Your", Color.green);
                    UiController.SetActionsText("Actions left: " + NumberOfActions);
                }
                else
                {
                    UiController.EnableActionButtons(false, null);
                    UiController.SetTurnText(turnBase.Actor + "s", Color.blue);
                    UiController.SetActionsText("");
                }
            }

            AngularRecieverController.Ready = true;
        }

        public void OnDestroy()
        {
            AngularSenderController.GetInstance().StopGameRequest();
        }


        public void OpenDoorRequest()
        {
            Debug.Log("OpenDoor called");
            // ANGULAR COMMUNICATION
            GameObject gameHero = HeroController.GetHeroGameObjectByName(UserHero);
            Door door = gameHero.GetComponent<CharacterModel>().TileModel.DoorModel.Door;
            DoorAction doorAction = new DoorAction(UserHero, door, ActionType.DOOR);
            AngularSenderController.GetInstance().OpenDoorRequest(doorAction);


            // TESTING
            /*var openDoorRoom2Json = "{\"actor\":\"Regdar\",\"actionType\":\"DOOR\",\"newRoom\":{\"name\":\"D1R2\",\"tiles\":[{\"id\":567,\"tileType\":\"TOPLEFT\",\"x\":11,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":568,\"tileType\":\"TOP\",\"x\":12,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":569,\"tileType\":\"TOP\",\"x\":13,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":570,\"tileType\":\"TOP\",\"x\":14,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":571,\"tileType\":\"TOP\",\"x\":15,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":572,\"tileType\":\"TOPRIGHT\",\"x\":16,\"y\":0,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":573,\"tileType\":\"LEFT\",\"x\":11,\"y\":1,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":574,\"tileType\":\"FLOOR\",\"x\":12,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":575,\"tileType\":\"FLOOR\",\"x\":13,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":576,\"tileType\":\"FLOOR\",\"x\":14,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":577,\"tileType\":\"FLOOR\",\"x\":15,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":578,\"tileType\":\"RIGHT\",\"x\":16,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":579,\"tileType\":\"LEFT\",\"x\":11,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":580,\"tileType\":\"FLOOR\",\"x\":12,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":581,\"tileType\":\"FLOOR\",\"x\":13,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":582,\"tileType\":\"FLOOR\",\"x\":14,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":583,\"tileType\":\"FLOOR\",\"x\":15,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":584,\"tileType\":\"RIGHT\",\"x\":16,\"y\":2,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":585,\"tileType\":\"FLOOR\",\"x\":11,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":586,\"tileType\":\"FLOOR\",\"x\":12,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":587,\"tileType\":\"FLOOR\",\"x\":13,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":588,\"tileType\":\"FLOOR\",\"x\":14,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":589,\"tileType\":\"FLOOR\",\"x\":15,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":590,\"tileType\":\"RIGHT\",\"x\":16,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":591,\"tileType\":\"LEFT\",\"x\":11,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":592,\"tileType\":\"FLOOR\",\"x\":12,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":593,\"tileType\":\"FLOOR\",\"x\":13,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":594,\"tileType\":\"FLOOR\",\"x\":14,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":595,\"tileType\":\"FLOOR\",\"x\":15,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":596,\"tileType\":\"RIGHT\",\"x\":16,\"y\":4,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":597,\"tileType\":\"LEFT\",\"x\":11,\"y\":5,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":598,\"tileType\":\"FLOOR\",\"x\":12,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":599,\"tileType\":\"FLOOR\",\"x\":13,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":600,\"tileType\":\"FLOOR\",\"x\":14,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":601,\"tileType\":\"FLOOR\",\"x\":15,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":602,\"tileType\":\"RIGHT\",\"x\":16,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":603,\"tileType\":\"BOTTOMLEFT\",\"x\":11,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":604,\"tileType\":\"BOTTOM\",\"x\":12,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":605,\"tileType\":\"BOTTOM\",\"x\":13,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":606,\"tileType\":\"BOTTOM\",\"x\":14,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":607,\"tileType\":\"BOTTOM\",\"x\":15,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":608,\"tileType\":\"BOTTOMRIGHT\",\"x\":16,\"y\":6,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false}],\"gameMonsters\":[{\"id\":1,\"symbolImg\":\"GoblinImage\",\"name\":\"Goblin\",\"description\":\"Description\",\"armor\":1,\"move\":5,\"undeadScore\":0,\"hitPoints\":4,\"maxHitPoints\":4,\"position\":{\"id\":589,\"tileType\":\"FLOOR\",\"x\":15,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}},{\"id\":2,\"symbolImg\":\"GoblinImage\",\"name\":\"Goblin\",\"description\":\"Description\",\"armor\":1,\"move\":5,\"undeadScore\":0,\"hitPoints\":4,\"maxHitPoints\":4,\"position\":{\"id\":600,\"tileType\":\"FLOOR\",\"x\":14,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}},{\"id\":3,\"symbolImg\":\"GoblinImage\",\"name\":\"Bugbear\",\"description\":\"Description\",\"armor\":1,\"move\":5,\"undeadScore\":0,\"hitPoints\":4,\"maxHitPoints\":4,\"position\":{\"id\":569,\"tileType\":\"TOP\",\"x\":13,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}}],\"doors\":[{\"id\":736,\"name\":\"R1R2\",\"tile1\":{\"id\":545,\"tileType\":\"FLOOR\",\"x\":10,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"tile2\":{\"id\":585,\"tileType\":\"FLOOR\",\"x\":11,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},\"enabled\":true,\"locked\":false}],\"starterRoom\":false},\"doorId\":736}";
            OpenDoorResponse(JsonConvert.DeserializeObject<DoorMessage>(openDoorRoom2Json));*/
        }

        private void OpenDoorResponse(DoorMessage doorMessage)
        {
            DungeonController.AddRoom(doorMessage.NewRoom);
            MonsterController.GenerateMonsters(doorMessage.NewRoom.GameMonsters);
            DungeonController.RemoveDoor(doorMessage.DoorId);
            DungeonController.SetTileTypeFloor(HeroController.GetHeroGameObjectByName(doorMessage.Actor)
                .GetComponent<CharacterModel>().TileModel.gameObject);
            UpdateUiActionsLeftAndButtons(UserHero);
            //HeroController.CheckFOV();
            AngularRecieverController.Ready = true;
        }

        public void GetRange()
        {
            Debug.Log("Get range");
            // ANGULAR COMMUNICATION
            AngularSenderController.GetInstance().GetRangeRequest(UserHero);

            // TESTING
            /*var JsonRange = "[{\"id\":542,\"tileType\":\"FLOOR\",\"x\":7,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":603,\"tileType\":\"BOTTOMLEFT\",\"x\":11,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":591,\"tileType\":\"LEFT\",\"x\":11,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":593,\"tileType\":\"FLOOR\",\"x\":13,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":529,\"tileType\":\"TOP\",\"x\":9,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":585,\"tileType\":\"FLOOR\",\"x\":11,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":541,\"tileType\":\"TOP\",\"x\":6,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":598,\"tileType\":\"FLOOR\",\"x\":12,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":534,\"tileType\":\"RIGHT\",\"x\":10,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":550,\"tileType\":\"FLOOR\",\"x\":8,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":592,\"tileType\":\"FLOOR\",\"x\":12,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":543,\"tileType\":\"FLOOR\",\"x\":8,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":535,\"tileType\":\"LEFT\",\"x\":7,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":581,\"tileType\":\"FLOOR\",\"x\":13,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":544,\"tileType\":\"FLOOR\",\"x\":9,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"PIT\",\"door\":false},{\"id\":587,\"tileType\":\"FLOOR\",\"x\":13,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":532,\"tileType\":\"FLOOR\",\"x\":8,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":565,\"tileType\":\"BOTTOM\",\"x\":9,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":545,\"tileType\":\"FLOOR\",\"x\":10,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":true},{\"id\":566,\"tileType\":\"BOTTOMRIGHT\",\"x\":10,\"y\":6,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":573,\"tileType\":\"LEFT\",\"x\":11,\"y\":1,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":567,\"tileType\":\"TOPLEFT\",\"x\":11,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":558,\"tileType\":\"FLOOR\",\"x\":9,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"PIT\",\"door\":false},{\"id\":580,\"tileType\":\"FLOOR\",\"x\":12,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":530,\"tileType\":\"TOPRIGHT\",\"x\":10,\"y\":0,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":579,\"tileType\":\"LEFT\",\"x\":11,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":586,\"tileType\":\"FLOOR\",\"x\":12,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":536,\"tileType\":\"FLOOR\",\"x\":8,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":549,\"tileType\":\"FLOOR\",\"x\":7,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"PIT\",\"door\":false},{\"id\":557,\"tileType\":\"FLOOR\",\"x\":8,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":533,\"tileType\":\"FLOOR\",\"x\":9,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":551,\"tileType\":\"FLOOR\",\"x\":9,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":559,\"tileType\":\"RIGHT\",\"x\":10,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":574,\"tileType\":\"FLOOR\",\"x\":12,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":588,\"tileType\":\"FLOOR\",\"x\":14,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":538,\"tileType\":\"RIGHT\",\"x\":10,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":552,\"tileType\":\"RIGHT\",\"x\":10,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":597,\"tileType\":\"LEFT\",\"x\":11,\"y\":5,\"pillar\":false,\"chest\":true,\"trapType\":\"NONE\",\"door\":false},{\"id\":537,\"tileType\":\"FLOOR\",\"x\":9,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}]";
            SetRange(JsonConvert.DeserializeObject<List<Tile>>(JsonRange));*/
        }

        public void SetRange(List<Tile> range)
        {
            Debug.Log("set range jeey");
            MovementController.SetupMove(range,
                HeroController.GetHeroGameObjectByName(UserHero).GetComponent<CharacterModel>().GameHero.Move,
                HeroController.GetHeroGameObjectByName(UserHero).GetComponent<CharacterModel>().TileModel.gameObject);
        }

        public void MoveHeroRequest()
        {
            Debug.Log("Move range");
            MovementController.ClearRangeTiles();
            // ANGULAR COMMUNICATION
            AngularSenderController.GetInstance().MoveRequest(new MoveAction(UserHero,
                MovementController.MovePath, ActionType.MOVE));

            // TESTING
            /*var moveJsonExample =
                "{\"actor\":\"Regdar\",\"actionType\":\"MOVE\",\"movement\":[{\"id\":516,\"tileType\":\"FLOOR\",\"x\":8,\"y\":1,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":520,\"tileType\":\"FLOOR\",\"x\":8,\"y\":2,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":527,\"tileType\":\"FLOOR\",\"x\":8,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},{\"id\":534,\"tileType\":\"FLOOR\",\"x\":8,\"y\":4,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}]}";
            MoveHeroResponse(new MoveMessage(){Actor = CurrentHero,Movement = MovementController.MovePath,MessageType = MessageType.MOVE});
        */
        }

        public void MoveHeroResponse(MoveMessage moveMessage)
        {
            string actor = moveMessage.Actor;
            var isNumeric = int.TryParse(actor, out int n);
            if (isNumeric)
            {
                StartCoroutine(MovementController.MoveTargetOverPath(
                    MonsterController.FindGameMonsterByNameId(n),
                    moveMessage.Movement));
            }
            else
            {
                StartCoroutine(MovementController.MoveTargetOverPath(
                    HeroController.GetHeroGameObjectByName(moveMessage.Actor),
                    moveMessage.Movement));
            }
        }

        public void TestTrap()
        {
            var trapJson =
                "{\"actor\":\"Regdar\",\"actionType\":\"TRAP\",\"tile\":{\"id\":963,\"tileType\":\"FLOOR\",\"x\":9,\"y\":3,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false},\"damage\":1}";
            TrapResponse(JsonConvert.DeserializeObject<TrapMessage>(trapJson));
        }

        public void TrapResponse(TrapMessage trapMessage)
        {
            HeroController.DamageGameHero(trapMessage.Actor, trapMessage.Damage, false);
            if (trapMessage.Actor.Equals(UserHero))
            {
                UiController.UpdateHeroStats(HeroController.GetHeroGameObjectByName(UserHero)
                    .GetComponent<CharacterModel>().GameHero);
            }

            DungeonController.DisplayTrap(trapMessage.Tile);
            AngularRecieverController.Ready = true;
        }

        public void OpenChestRequest()
        {
            // ANGULAR COMMUNICATION
            AngularSenderController.GetInstance().OpenChestRequest(new ChestAction(UserHero, ActionType.CHEST,
                HeroController.GetHeroGameObjectByName(UserHero).GetComponent<CharacterModel>().TileModel.Tile));
            // TESTING
            /*var openChestJson = "{\"actor\":\"Regdar\",\"actionType\":\"CHEST\",\"treasure\":{\"id\":22,\"name\":\"Short bow of the ancients\",\"description\":\"Description\",\"img\":\"image\",\"level\":1,\"startersEquipment\":false,\"type\":\"Weapon\"},\"damage\":0,\"tile\":{\"id\":597,\"tileType\":\"LEFT\",\"x\":11,\"y\":5,\"pillar\":false,\"chest\":false,\"trapType\":\"NONE\",\"door\":false}}";
            OpenChestResponse(JsonConvert.DeserializeObject<ChestMessage>(openChestJson));*/
        }

        public void OpenChestResponse(ChestMessage chestMessage)
        {
            DungeonController.RemoveChest(chestMessage.Tile);
            if (chestMessage.Damage == 0 && chestMessage.Actor.Equals(UserHero))
            {
                ItemController.AddTreasureToKnapsack(chestMessage.Treasure);
            }
            else
            {
                HeroController.DamageGameHero(chestMessage.Actor, chestMessage.Damage, false);
                if (chestMessage.Actor.Equals(UserHero))
                {
                    UiController.UpdateHeroStats(HeroController.GetHeroGameObjectByName(UserHero)
                        .GetComponent<CharacterModel>().GameHero);
                }
            }

            UpdateUiActionsLeftAndButtons(UserHero);
            AngularRecieverController.Ready = true;
        }

        private int TestAttackTimes = 0;

        public void AttackRequest(long targetId, long itemId)
        {
            // ANGULAR COMMUNICATION
            AngularSenderController.GetInstance()
                .AttackRequest(new AttackAction(UserHero, ActionType.ATTACK, targetId, itemId));
            // TESTING
            /*Debug.Log("targetId and ItemName: " + targetId + " / " + itemId);
            var AttackJson1 = "{\"actor\":\"Regdar\",\"actionType\":\"ATTACK\",\"targedId\":2,\"damage\":2,\"dead\":false}";
            var AttackJson2 = "{\"actor\":\"Regdar\",\"actionType\":\"ATTACK\",\"targedId\":2,\"damage\":3,\"dead\":true}";
            TestAttackTimes++;
            if (TestAttackTimes == 1)
            {
                AttackResponse(JsonConvert.DeserializeObject<AttackMessage>(AttackJson1));
            }
            if (TestAttackTimes == 2)
            {
                AttackResponse(JsonConvert.DeserializeObject<AttackMessage>(AttackJson2));
            }*/
        }

        public void AttackResponse(AttackMessage attackMessage)
        {
            string actor = attackMessage.Actor;
            var isNumeric = int.TryParse(actor, out int n);
            if (!isNumeric)
            {
                // Hero
                MonsterController.DamageGameMonster(attackMessage.TargetId, attackMessage.Damage, attackMessage.Dead);
                UpdateUiActionsLeftAndButtons(UserHero);
                HeroController.GetHeroGameObjectByName(actor).GetComponent<Animator>().SetTrigger("Attack");
            }
            else
            {
                // Dungeon Master
                string heroName = HeroController.GetHeroGameObjectById(attackMessage.TargetId).name;
                HeroController.DamageGameHero(heroName, attackMessage.Damage, attackMessage.Dead);
                if (heroName.Equals(UserHero))
                {
                    UiController.UpdateHeroStats(HeroController.GetHeroGameObjectByName(UserHero)
                        .GetComponent<CharacterModel>().GameHero);
                }
                MonsterController.GetMonsterByName(actor).GetComponent<Animator>().SetTrigger("Attack");
            }

            AngularRecieverController.Ready = true;
        }

        public void SpectatorWatchHero(string heroName)
        {
            CameraController.SetMainGameHero(HeroController.GetHeroGameObjectByName(heroName).transform);
        }

        public void EndGameResponse(bool endGame)
        {
            UiController.DisplayEndGame(endGame);
        }

        public void EndGameRequest()
        {
            AngularSenderController.GetInstance().StopGameRequest();
        }
    }
}