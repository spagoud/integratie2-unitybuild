﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.GameControllers;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    private long TargetId;

    private long ItemId;

    private GameController GameController;


    // Start is called before the first frame update
    void Start()
    {
        GameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTargetId(long targetId)
    {
        TargetId = targetId;
    }

    public bool CanHeroAttackMonster(Vector3 positionMonster, Vector3 positionHero)
    {
        if (positionMonster - new Vector3(1f, 0, 0) == positionHero)
        {
            return true;
        }
        if (positionMonster - new Vector3(-1f, 0, 0) == positionHero)
        {
            return true;
        }
        if (positionMonster - new Vector3(0, 0, 1f) == positionHero)
        {
            return true;
        }
        if (positionMonster - new Vector3(0, 0, -1f) == positionHero)
        {
            return true;
        }

        return false;
    }

    public void AttackMonsterSetup(long itemId)
    {
        GameController.AttackRequest(TargetId,itemId);
    }
}
