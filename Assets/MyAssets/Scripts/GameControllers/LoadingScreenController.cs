﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreenController : MonoBehaviour
{
    [SerializeField]
    private Transform LoadImg;
    [SerializeField]
    private Transform InnerLoadImg;
    [SerializeField]
    private Transform InnerInnerLoadImg;
    [SerializeField]
    private Transform InnerInnerInnerLoadImg;

    private float time;

    void Start()
    {
        time = 0;
    }

    void Update()
    {
        time += Time.deltaTime;
        LoadImg.GetComponent<Image>().fillAmount = time % 4;
        InnerLoadImg.GetComponent<Image>().fillAmount = time % 3;
        InnerInnerLoadImg.GetComponent<Image>().fillAmount = time % 2;
        InnerInnerInnerLoadImg.GetComponent<Image>().fillAmount = time % 1;
    }
}
