﻿using Assets.Scripts.Model;
using Assets.Scripts.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class DungeonController : MonoBehaviour
{
    public List<TileTypeObject> Tiles = Enum.GetValues(typeof(TileType)).Cast<TileType>().Select(type =>
    {
        TileTypeObject tileTypeObject = new TileTypeObject
        {
            tileType = type
        };
        return tileTypeObject;
    }).ToList();

    public GameObject ChestPrefab;
    public GameObject PillarPrefab;
    public GameObject DoorPrefab;
    public GameObject TrapPrefab;

    public GameObject RoomsCollection;
    public GameObject ChestsCollection;
    public GameObject PillarsCollection;
    public GameObject DoorsCollection;
    public GameObject TrapCollection;

    public List<Room> Rooms { get; set; }
    private MoveToFitObject MoveToFitObject;
    private int NumberOfChests;
    private int NumberOfPillars;
    private int NumberOfTraps;

    public List<GameObject> GameTiles { get; set; }
    public List<GameObject> GameDoors { get; set; }

    public void AddRoom(Room room)
    {
        MoveToFitObject = FindObjectsOfType<MoveToFitObject>()[0];
        MoveToFitObject.centerPoint = RoomsCollection.transform;
        Rooms.Add(room);
        generateRoom(room);
    }

    // Start is called before the first frame update
    void Start()
    {
        Rooms = new List<Room>();
        NumberOfChests = 0;
        NumberOfPillars = 0;
        NumberOfTraps = 0;
        GameTiles = new List<GameObject>();
        GameDoors = new List<GameObject>();
    }

    private void generateRoom(Room room)
    {
        GameObject roomObject = new GameObject("Room: " + room.Name);
        roomObject.transform.position = new Vector3();
        roomObject.transform.SetParent(RoomsCollection.transform, true);
        foreach (var tile in room.Tiles)
        {
            GameObject gameTile;
            if (tile.Door)
            {
                gameTile = Instantiate(new List<TileTypeObject>(Tiles).Find(tto => tto.tileType == TileType.FLOOR)
                    .gameObject);
            }
            else
            {
                gameTile = Instantiate(new List<TileTypeObject>(Tiles).Find(tto => tto.tileType == tile.TileType)
                    .gameObject);
            }

            gameTile.transform.position = new Vector3(tile.X, 0, -tile.Y);
            gameTile.name = tile.Id.ToString();
            gameTile.transform.SetParent(roomObject.transform, true);
            gameTile.GetComponent<TileModel>().Tile = tile;
            GameTiles.Add(gameTile);

            if (tile.Chest)
            {
                NumberOfChests++;
                GameObject gameChest = Instantiate(ChestPrefab.gameObject);
                gameChest.transform.position = new Vector3(tile.X, 0, -tile.Y);
                gameChest.name = "CHEST" + NumberOfChests;
                gameChest.transform.SetParent(ChestsCollection.transform, true);
                RotateChests(tile, gameChest);
                gameTile.GetComponent<TileModel>().ChestGameObject = gameChest;
            }

            if (tile.Pillar)
            {
                NumberOfPillars++;
                GameObject gamePillar = Instantiate(PillarPrefab.gameObject);
                gamePillar.transform.position = new Vector3(tile.X, 0f, -tile.Y);
                gamePillar.name = "PILLAR" + NumberOfPillars;
                gamePillar.transform.SetParent(PillarsCollection.transform, true);
            }

            // for found traps
            /*if (tile.TrapType == TrapType.PIT)
            {
                NumberOfTraps++;
                GameObject gameTrap = Instantiate(TrapPrefab.gameObject);
                gameTrap.transform.position = new Vector3(tile.X, 0.05f, -tile.Y);
                gameTrap.name = "TRAP" + NumberOfTraps;
                gameTrap.transform.SetParent(TrapCollection.transform, true);
            }*/
        }

        foreach (var door in room.Doors)
        {
            if (!GameDoors.Find(gd => gd.name.Equals(door.Id.ToString())))
            {
                GameObject gameDoor = Instantiate(DoorPrefab.gameObject);
                float xCoord = (door.Tile1.X + door.Tile2.X) / 2f;
                float yCoord = (door.Tile1.Y + door.Tile2.Y) / 2f;
                gameDoor.transform.position = new Vector3(xCoord, 0.5f, -yCoord);
                gameDoor.name = door.Id.ToString();
                gameDoor.transform.SetParent(DoorsCollection.transform, true);
                if (!(xCoord - door.Tile1.X < 0.001))
                {
                    gameDoor.transform.Rotate(0, 90, 0);
                }

                gameDoor.GetComponent<DoorModel>().Door = door;
                GameDoors.Add(gameDoor);
                if (GameTiles.Exists(t => t.name.Equals(door.Tile1.Id.ToString())))
                {
                    GameTiles.Find(t => t.name.Equals(door.Tile1.Id.ToString())).GetComponent<TileModel>().DoorModel =
                        gameDoor.GetComponent<DoorModel>();
                }

                if (GameTiles.Exists(t => t.name.Equals(door.Tile2.Id.ToString())))
                {
                    GameTiles.Find(t => t.name.Equals(door.Tile2.Id.ToString())).GetComponent<TileModel>().DoorModel =
                        gameDoor.GetComponent<DoorModel>();
                }
            }
        }
    }

    private void RotateChests(Tile tile, GameObject gameChest)
    {
        if (tile.TileType.ToString().Equals("TOP"))
        {
            gameChest.transform.Rotate(0, 180, 0);
        }
        else if (tile.TileType.ToString().Equals("RIGHT"))
        {
            gameChest.transform.Rotate(0, -90, 0);
        }
        else if (tile.TileType.ToString().Equals("LEFT"))
        {
            gameChest.transform.Rotate(0, 90, 0);
        }
        else if (tile.TileType.ToString().Equals("BOTTOMLEFT"))
        {
            gameChest.transform.Rotate(0, 45, 0);
        }
        else if (tile.TileType.ToString().Equals("BOTTOMRIGHT"))
        {
            gameChest.transform.Rotate(0, -45, 0);
        }
        else if (tile.TileType.ToString().Equals("TOPRIGHT"))
        {
            gameChest.transform.Rotate(0, -135, 0);
        }
        else if (tile.TileType.ToString().Equals("TOPLEFT"))
        {
            gameChest.transform.Rotate(0, 135, 0);
        }
    }

    public GameObject GetGameTileByPosition(Vector3 position)
    {
        return GameTiles.Find(gt => gt.transform.position == position);
    }

    public GameObject FindGameTileByIdName(long id)
    {
        return GameTiles.Find(g => g.name.Equals(id.ToString()));
    }

    public GameObject FindGameDoorByIdName(long id)
    {
        return GameDoors.Find(g => g.name.Equals(id.ToString()));
    }

    public void RemoveDoor(long doorId)
    {
        GameObject gameDoor = GameDoors.Find(g => g.name.Equals(doorId.ToString()));
        gameDoor.SetActive(false);
    }

    public void SetTileTypeFloor(GameObject gameTile)
    {
        gameTile.GetComponent<TileModel>().Tile.TileType = TileType.FLOOR;
        gameTile.GetComponent<TileModel>().Tile.Door = false;
    }

    public void DisplayTrap(Tile tile)
    {
        NumberOfTraps++;
        GameObject gameTrap = Instantiate(TrapPrefab.gameObject);
        gameTrap.transform.position = new Vector3(tile.X, 0.05f, -tile.Y);
        gameTrap.name = "TRAP" + NumberOfTraps;
        gameTrap.transform.SetParent(TrapCollection.transform, true);
    }

    public void RemoveChest(Tile tile)
    {
        TileModel tileModel = FindGameTileByIdName(tile.Id).GetComponent<TileModel>();
        tileModel.ChestGameObject.SetActive(false);
        tileModel.Tile.Chest = false;
    }
}

[Serializable]
public class TileTypeObject
{
    public TileType tileType;
    public GameObject gameObject;
}