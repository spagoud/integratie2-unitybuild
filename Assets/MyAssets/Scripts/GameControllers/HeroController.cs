﻿using Assets.Scripts.Model.Enums;
using Assets.Scripts.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameControllers
{
    public class HeroController : MonoBehaviour
    {
        private DungeonController DungeonController;
        private MonsterController MonsterController;

        public List<HeroObjectMap> Heroes;

        public GameObject GameHeroTest;

        public GameObject HeroCollection;

        // moving extra
        private bool CanMove;
        public List<GameObject> GameHeroes { get; set; }

        public void Start()
        {
            DungeonController = GetComponent<DungeonController>();
            MonsterController = GetComponent<MonsterController>();
            CanMove = true;
            GameHeroes = new List<GameObject>();
        }

        public void GenerateHeroes(List<GameHero> heroes)
        {
            foreach (var hero in heroes)
            {
                GameObject gameHero =
                    Instantiate(Heroes.Find(gho => gho.HeroType.Equals(hero.Type.ToUpper())).GameObject);
                gameHero.transform.position = new Vector3(hero.Position.X, 0f, -hero.Position.Y);
                gameHero.name = hero.HeroName;
                gameHero.transform.SetParent(HeroCollection.transform, true);
                gameHero.GetComponent<CharacterModel>().GameHero = hero;
                gameHero.GetComponent<CharacterModel>().TileModel = DungeonController.GameTiles
                    .Find(t => t.name.Equals(hero.Position.Id.ToString())).GetComponent<TileModel>();
                GameHeroes.Add(gameHero);
            }
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.DownArrow) && CanMove)
            {
                CanMove = false;
                GameHeroes[0].transform.position = (GameHeroes[0].transform.position - new Vector3(0, 0, 1));
            }

            if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                CanMove = true;
            }

            if (Input.GetKeyDown(KeyCode.UpArrow) && CanMove)
            {
                CanMove = false;
                GameHeroes[0].transform.position = (GameHeroes[0].transform.position - new Vector3(0, 0, -1));
            }

            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                CanMove = true;
            }

            if (Input.GetKeyDown(KeyCode.RightArrow) && CanMove)
            {
                CanMove = false;
                GameHeroes[0].transform.position = (GameHeroes[0].transform.position - new Vector3(-1, 0, 0));
            }

            if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                CanMove = true;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow) && CanMove)
            {
                CanMove = false;
                GameHeroes[0].transform.position = (GameHeroes[0].transform.position - new Vector3(1, 0, 0));
            }

            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                CanMove = true;
            }
        }

        public GameObject GetHeroGameObjectById(long heroId)
        {
            return GameHeroes.Find(gh => gh.GetComponent<CharacterModel>().GameHero.Id == heroId);
        }

        public GameObject GetHeroGameObjectByName(string heroName)
        {
            try
            {
                return HeroCollection.transform.Find(heroName).gameObject;
            }
            catch (Exception e)
            {
                try
                {
                    return HeroCollection.transform.Find(heroName+"AI").gameObject;
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }
            }

            return null;
        }

        public void DamageGameHero(string actor, int damage, bool dead)
        {
            int health = GetHeroGameObjectByName(actor).GetComponent<CharacterModel>().GameHero.HealthPoints -= damage;
            if (dead || health <= 0)
            {
                GetHeroGameObjectByName(actor).GetComponent<Animator>().SetTrigger("Die");
            }
        }

        public void CheckFOV()
        {
            StartCoroutine(checkFOVRoutine());
        }

        private IEnumerator checkFOVRoutine()
        {
            var layermask = (1 << 10) | (1 << 11);
            RaycastHit hit;
            MonsterController.GameMonsters.ForEach(gameMonster =>
            {
                gameMonster.gameObject.GetComponentsInChildren<Renderer>().ToList()
                    .ForEach(renderer => renderer.enabled = false);
            });
            yield return null;
            GameHeroes.ForEach((gamehero) =>
            {
                Vector3 origin = gamehero.transform.position;

                MonsterController.GameMonsters.ForEach(gameMonster =>
                {
                    Debug.DrawRay(origin, gameMonster.transform.position - origin, Color.cyan, 20, true);
                    Physics.Raycast(origin, gameMonster.transform.position - origin, out hit, float.MaxValue,
                        layermask);
                    if (hit.collider)
                    {
                        if (hit.collider.gameObject.transform.parent.name.Equals(gameMonster.name))
                        {
                            gameMonster.gameObject.GetComponentsInChildren<Renderer>().ToList()
                                .ForEach(renderer => renderer.enabled = true);
                        }
                    }
                });
            });
        }
    }

    [Serializable]
    public class HeroObjectMap
    {
        public string HeroType;
        public GameObject GameObject;
    }
}