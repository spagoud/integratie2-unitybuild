﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.GameControllers;
using Assets.Scripts.Model;
using Assets.Scripts.Model.Enums;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MovementController : MonoBehaviour
{
    private DungeonController DungeonController;
    private CameraController CameraController;
    private UiController UiController;
    private GameController GameController;
    private Color RangeColor;
    private Color ClickableColor;
    private Color PathColor;
    private List<GameObject> GameTilesRange;
    private List<Tile> TilesRange;
    public List<Tile> MovePath { get; set; }
    private int MovesLeft = 0;
    public List<GameObject> GameTileSteps { get; set; }
    public float TimeToMoveTarget = 1f;
    public float TimeToRotateTarget = 1f;

    void Start()
    {
        DungeonController = GetComponent<DungeonController>();
        CameraController = GetComponent<CameraController>();
        UiController = GetComponent<UiController>();
        GameController = GetComponent<GameController>();
        RangeColor = new Color(0.15f, 0.15f, 0.1f);
        ClickableColor = new Color(0.3f, 0.3f, 0.2f);
        PathColor = new Color(0.4f, 0.4f, 0.4f);
        GameTilesRange = new List<GameObject>();
        TilesRange = new List<Tile>();
        MovePath = new List<Tile>();
        GameTileSteps = new List<GameObject>();
    }


    public void SetupMove(List<Tile> range, int movesLeft, GameObject StartPoint)
    {
        GameTilesRange.Clear();
        MovePath.Clear();
        GameTileSteps.Clear();
        TilesRange = range;
        foreach (var tile in range)
        {
            GameObject gameTile = FindTileByID(tile.Id);
            GameTilesRange.Add(gameTile);
            MakeTileRange(gameTile);
        }

        MovesLeft = movesLeft + 1;
        AddTileToMovePath(StartPoint);
        SetMoveableArea();
    }

    private void ResetRange()
    {
        foreach (GameObject gameTile in GameTilesRange)
        {
            if (!GameTileSteps.Exists(gts => gts == gameTile))
            {
                MakeTileRange(gameTile);
            }
        }
    }

    public void SetMoveableArea()
    {
        ResetRange();
        if (MovesLeft > 0)
        {
            GameObject gameTileStep = GameTileSteps[GameTileSteps.Count - 1];
            if (!gameTileStep.GetComponent<TileModel>().Tile.TileType.ToString().Contains("TOP"))
            {
                GameObject gameTile =
                    DungeonController.GetGameTileByPosition(gameTileStep.transform.position + new Vector3(0f, 0f, 1f));
                if (GameTilesRange.Exists(gt => gt == gameTile) && !GameTileSteps.Exists(gts => gts == gameTile))
                {
                    MakeTileClickable(gameTile);
                }
            }

            if (!gameTileStep.GetComponent<TileModel>().Tile.TileType.ToString().Contains("BOTTOM"))
            {
                GameObject gameTile =
                    DungeonController.GetGameTileByPosition(gameTileStep.transform.position + new Vector3(0f, 0f, -1f));
                if (GameTilesRange.Exists(gt => gt == gameTile) && !GameTileSteps.Exists(gts => gts == gameTile))
                {
                    MakeTileClickable(gameTile);
                }
            }

            if (!gameTileStep.GetComponent<TileModel>().Tile.TileType.ToString().Contains("LEFT"))
            {
                GameObject gameTile =
                    DungeonController.GetGameTileByPosition(gameTileStep.transform.position + new Vector3(-1f, 0f, 0f));
                if (GameTilesRange.Exists(gt => gt == gameTile) && !GameTileSteps.Exists(gts => gts == gameTile))
                {
                    MakeTileClickable(gameTile);
                }
            }

            if (!gameTileStep.GetComponent<TileModel>().Tile.TileType.ToString().Contains("RIGHT"))
            {
                GameObject gameTile =
                    DungeonController.GetGameTileByPosition(gameTileStep.transform.position + new Vector3(1f, 0f, 0f));
                if (GameTilesRange.Exists(gt => gt == gameTile) && !GameTileSteps.Exists(gts => gts == gameTile))
                {
                    MakeTileClickable(gameTile);
                }
            }
        }
    }

    private GameObject FindTileByID(long id)
    {
        foreach (GameObject gameTile in DungeonController.GameTiles)
        {
            if (gameTile.name.Equals(id.ToString()))
            {
                return gameTile;
            }
        }

        return new GameObject();
    }

    public void ClearRangeTiles()
    {
        foreach (GameObject gameTile in GameTilesRange)
        {
            MakeTileNormal(gameTile);
        }

        CameraController.ResetCamera();
    }

    private void MakeTileRange(GameObject tile)
    {
        tile.GetComponent<MouseResponse>().enabled = false;
        tile.GetComponent<Renderer>().material.SetColor("_EmissionColor", RangeColor);
    }

    private void MakeTileClickable(GameObject tile)
    {
        tile.GetComponent<MouseResponse>().enabled = true;
        tile.GetComponent<Renderer>().material.SetColor("_EmissionColor", ClickableColor);
    }

    private void MakeTileNormal(GameObject tile)
    {
        tile.GetComponent<MouseResponse>().enabled = false;
        tile.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
    }

    private void MakeTilePath(GameObject tile)
    {
        tile.GetComponent<MouseResponse>().enabled = false;
        tile.GetComponent<Renderer>().material.SetColor("_EmissionColor", PathColor);
    }

    public void AddTileToMovePath(GameObject gameTile)
    {
        if (MovesLeft > 0) //probably not needed
        {
            foreach (Tile tile in TilesRange)
            {
                if (tile.Id.ToString().Equals(gameTile.name) &&
                    !MovePath.Exists(t => t.Id.ToString().Equals(gameTile.name)))
                {
                    MovePath.Add(tile);
                    MovesLeft--;
                    GameTileSteps.Add(gameTile);
                    MakeTilePath(gameTile);
                    UiController.UpdateMoveInfo("moves: " + MovesLeft);
                }
            }
        }
    }

    public IEnumerator MoveTargetOverPath(GameObject target, List<Tile> MovePath)
    {
        if (MovePath.Count > 1)
        {
            target.gameObject.GetComponent<Animator>().SetTrigger("Walk");
            foreach (Tile tile in MovePath)
            {
                target.GetComponent<CharacterModel>().TileModel =
                    DungeonController.FindGameTileByIdName(tile.Id).GetComponent<TileModel>();
                Vector3 movePosition = new Vector3(tile.X, target.transform.position.y, -tile.Y);
                if (target.transform.position != movePosition)
                {
                    yield return MoveAnimation(target, movePosition);
                }
            }

            target.gameObject.GetComponent<Animator>().SetTrigger("Walk");
            //GameController.HeroController.CheckFOV();
        }

        GameController.UpdateUiActionsLeftAndButtons(target.name);
        AngularRecieverController.Ready = true;
    }

    private IEnumerator MoveAnimation(GameObject target, Vector3 movePosition)
    {
        var currentPos = target.transform.position;
        var t = 0f;
        Vector3 dir = movePosition - currentPos;
        Quaternion rot = Quaternion.LookRotation(dir);
        while (target.transform.rotation != rot && t < 1)
        {
            t += Time.deltaTime / TimeToRotateTarget;
            t = (t > 1 ? 1 : t);
            target.transform.rotation = Quaternion.Slerp(target.transform.rotation, rot, t);
            yield return null;
        }

        t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / TimeToMoveTarget;
            t = (t > 1 ? 1 : t);
            target.transform.position = Vector3.Lerp(currentPos, movePosition, t);
            yield return null;
        }
    }
}