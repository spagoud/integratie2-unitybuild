﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Assets.Scripts.GameControllers;
using Assets.Scripts.Model;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{
    [SerializeField] private Text StoryDescriptionUIText;
    [SerializeField] private GameObject LoadingScreen;
    [SerializeField] private GameObject StartStoryMenu;
    [SerializeField] private GameObject MainMenu;
    [SerializeField] private GameObject SequenceDisplay;
    [SerializeField] private GameObject TreasureInfoDisplay;
    [SerializeField] private GameObject MonsterInfoDisplay;
    [SerializeField] private GameObject HeroInfoDisplay;
    [SerializeField] private GameObject SpectatorDisplay;
    [SerializeField] private GameObject EndGameDisplay;
    private Transform HeroStats;

    public List<ImageObjectMap> ImageObjectMaps;

    public GameObject MoveButton;
    public GameObject OpenDoorButton;
    public GameObject OpenChestButton;
    public GameObject SequenceButton;
    public GameObject AttackButton;

    private GameController GameController;

    public void Start()
    {
        LoadingScreen.SetActive(true);
        StartStoryMenu.SetActive(false);

        HeroStats = MainMenu.transform.Find("HeroDisplay").Find("HeroStats");
        GameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    public void SetStoryDescription(string description)
    {
        StoryDescriptionUIText.text = description;
        StartStoryMenu.SetActive(true);
    }

    public void DisableLoadingScreen()
    {
        LoadingScreen.SetActive(false);
    }

    public void DisplaySequence(List<string> sequencePlayerSequence)
    {
        SequenceDisplay.SetActive(true);
        StringBuilder stringBuilder = new StringBuilder();
        foreach (string s in sequencePlayerSequence)
        {
            stringBuilder.Append(s + "\n");
        }

        Transform sequenceUi = SequenceDisplay.transform.Find("Background").Find("Sequence");
        if (sequenceUi != null)
        {
            sequenceUi.GetComponent<Text>().text = stringBuilder.ToString();
        }
        else
        {
            Debug.Log("sequence UI NOT found");
        }
    }

    public void SetHeroNameText(string heroName)
    {
        MainMenu.transform.Find("HeroDisplay").Find("HeroStats").Find("HeroText").Find("Hero").GetComponent<Text>()
            .text = heroName;
    }

    public void SetTurnText(string turnName, Color textColor)
    {
        MainMenu.transform.Find("HeroDisplay").Find("HeroStats").Find("HeroText").Find("Turn").GetComponent<Text>()
                .text = turnName + " turn";
        MainMenu.transform.Find("HeroDisplay").Find("HeroStats").Find("HeroText").Find("Turn").GetComponent<Text>()
            .color = textColor;
    }

    public void SetActionsText(string actionAmount)
    {
        MainMenu.transform.Find("HeroDisplay").Find("HeroStats").Find("HeroText").Find("ActionsLeft")
            .GetComponent<Text>()
            .text = actionAmount;
    }

    public void DisplayMainMenu(bool enableDisplay)
    {
        MainMenu.SetActive(enableDisplay);
    }

    public void EnableActionButtons(bool enableButtons, Tile tile)
    {
        foreach (Transform button in MainMenu.transform.Find("ActionButtons").transform)
        {
            button.GetComponent<Button>().interactable = false;
        }
        GameController.ItemController.EnableTreasureButtons(enableButtons);

        SequenceButton.GetComponent<Button>().interactable = true;
        if (enableButtons)
        {
            MoveButton.GetComponent<Button>().interactable = true;
            if (tile.Chest)
            {
                OpenChestButton.GetComponent<Button>().interactable = true;
            }

            if (tile.Door)
            {
                OpenDoorButton.GetComponent<Button>().interactable = true;
            }
        }
    }

    public void EnableAttackButton(bool enableButton)
    {
        AttackButton.GetComponent<Button>().interactable = enableButton;
    }

    public void UpdateHeroStats(GameHero gameHero)
    {
        MainMenu.transform.Find("HeroDisplay").Find("HeroStats").Find("Life").GetComponentInChildren<Text>().text =
            gameHero.HealthPoints + "/" + gameHero.MaxHealthPoints + " Health";
        MainMenu.transform.Find("HeroDisplay").Find("HeroStats").Find("Life").GetComponent<Image>().fillAmount =
            (float) gameHero.HealthPoints / gameHero.MaxHealthPoints;
        MainMenu.transform.Find("HeroDisplay").Find("HeroStats").Find("Mana").GetComponentInChildren<Text>().text =
            gameHero.MagicPoints + "/" + gameHero.MaxMagicPoints + " Magic";
        MainMenu.transform.Find("HeroDisplay").Find("HeroStats").Find("Mana").GetComponent<Image>().fillAmount =
            (float) gameHero.MagicPoints / gameHero.MaxMagicPoints;
        MainMenu.transform.Find("HeroDisplay").Find("Image").GetComponent<Image>().sprite =
            ImageObjectMaps.Find(iom => iom.ImageName.Equals(gameHero.HeroName)).Sprite;
    }

    public void UpdateMoveInfo(string updateText)
    {
        MainMenu.transform.Find("MoveInfo").Find("Text").GetComponent<Text>().text = updateText;
    }

    public void EnableTreasureInfo(bool enableInfo)
    {
        TreasureInfoDisplay.SetActive(enableInfo);
    }

    public void EnableMonsterInfo(bool enableInfo)
    {
        MonsterInfoDisplay.SetActive(enableInfo);
    }

    public void SetMonsterDisplay(GameMonster gameMonster, bool canAttack)
    {
        MonsterInfoDisplay.transform.Find("InfoPanel").Find("MonsterStats").Find("Life").GetComponent<Image>()
                .fillAmount = (float) gameMonster.HitPoints / gameMonster.MaxHitPoints;
        MonsterInfoDisplay.transform.Find("InfoPanel").Find("MonsterStats").Find("Life").GetComponentInChildren<Text>()
                .text = gameMonster.HitPoints + "/" + gameMonster.MaxHitPoints + " Hitpoints";
        MonsterInfoDisplay.transform.Find("InfoPanel").Find("MonsterStats").Find("Description").GetComponent<Text>()
            .text = gameMonster.Description;
        MonsterInfoDisplay.transform.Find("InfoPanel").Find("MonsterStats").Find("MonsterText").Find("Monster")
            .GetComponent<Text>().text = gameMonster.Name;
        MonsterInfoDisplay.transform.Find("InfoPanel").Find("Image").GetComponent<Image>().sprite =
            ImageObjectMaps.Find(iom => iom.ImageName.Equals(gameMonster.Name)).Sprite;
        EnableAttackButton(canAttack);
    }

    public void SetTreasureDisplay(Item item)
    {
        TreasureInfoDisplay.transform.Find("InfoPanel").Find("Image").GetComponent<Image>().sprite =
            ImageObjectMaps.Find(iom => iom.ImageName.Equals(item.Type)).Sprite;
        TreasureInfoDisplay.transform.Find("InfoPanel").Find("Name").GetComponent<Text>().text = item.Name;
        TreasureInfoDisplay.transform.Find("InfoPanel").Find("Description").GetComponent<Text>().text =
            item.Description;
        //TODO: damage dislay
    }

    public void EnableHeroInfo(bool enableDisplay)
    {
        HeroInfoDisplay.SetActive(enableDisplay);
    }

    public void SetHeroInfoDisplay(GameHero gameHero)
    {
        HeroInfoDisplay.transform.Find("InfoPanel").Find("Life").GetComponent<Image>()
                .fillAmount = (float)gameHero.HealthPoints / gameHero.MaxHealthPoints;
        HeroInfoDisplay.transform.Find("InfoPanel").Find("Life").GetComponentInChildren<Text>()
                .text = gameHero.HealthPoints + "/" + gameHero.MaxHealthPoints + " Health";
        HeroInfoDisplay.transform.Find("InfoPanel").Find("Mana").GetComponent<Image>()
                .fillAmount = (float)gameHero.MagicPoints / gameHero.MaxMagicPoints;
        HeroInfoDisplay.transform.Find("InfoPanel").Find("Mana").GetComponentInChildren<Text>()
                .text = gameHero.MagicPoints + "/" + gameHero.MaxMagicPoints + " Magic";
        HeroInfoDisplay.transform.Find("InfoPanel").Find("HeroText").Find("Hero")
            .GetComponent<Text>().text = gameHero.HeroName;
        HeroInfoDisplay.transform.Find("InfoPanel").Find("Image").GetComponent<Image>().sprite =
            ImageObjectMaps.Find(iom => gameHero.HeroName.Contains(iom.ImageName)).Sprite;
    }

    public void EnableSpectatorDisplay(bool enableDisplay)
    {
        SpectatorDisplay.SetActive(enableDisplay);
    }

    public void DisplayEndGame(bool endGame)
    {
        EndGameDisplay.SetActive(true);
        EndGameDisplay.transform.Find("Title").GetComponent<Text>().text = "congratulations";
        EndGameDisplay.transform.Find("Info").GetComponent<Text>().text = "You completed the dungeon.";
    }
}

[Serializable]
public class ImageObjectMap
{
    public string ImageName;
    public Sprite Sprite;
}