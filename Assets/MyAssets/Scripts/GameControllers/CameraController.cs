﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform PlayerTransform;

    private Vector3 _cameraOffset;

    [Range(0.01f, 1.0f)] public float SmoothFactor = 0.5f;

    public bool LookAtPlayer = false;

    public bool RotateAroundPlayer = true;
    public bool MainHeroSet = false;

    public float RotationsSpeed = 5.0f;

    public void SetMainGameHero(Transform gameHero)
    {
        PlayerTransform = gameHero;
        Camera.main.transform.position = PlayerTransform.position + new Vector3(0f, 5.5f, -3f);
        Camera.main.transform.LookAt(PlayerTransform);
        _cameraOffset = Camera.main.transform.position - PlayerTransform.position;
        //Debug.Log("CameraController: " + _cameraOffset);
        MainHeroSet = true;
    }

    public void ResetCamera()
    {
        Camera.main.transform.position = PlayerTransform.position + _cameraOffset;
        Camera.main.transform.LookAt(PlayerTransform);
    }
    
    void Update()
    {
        if (MainHeroSet)
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                if (RotateAroundPlayer)
                {
                    Quaternion camTurnAngle =
                        Quaternion.AngleAxis(Input.GetAxis("Mouse X") * RotationsSpeed, Vector3.up);

                    _cameraOffset = camTurnAngle * _cameraOffset;
                    _cameraOffset += _cameraOffset * Input.GetAxis("Mouse ScrollWheel");
                    //Debug.Log("CameraController: " + _cameraOffset);
                }

                Vector3 newPos = PlayerTransform.position + _cameraOffset;

                Camera.main.transform.position = Vector3.Slerp(Camera.main.transform.position, newPos, SmoothFactor);

                if (LookAtPlayer || RotateAroundPlayer)
                    Camera.main.transform.LookAt(PlayerTransform);
            }
            else ResetCamera();
        }
    }

    public void MoveCamera(Vector3 translateVector)
    {
        Camera.main.transform.position = _cameraOffset + translateVector;
    }
}