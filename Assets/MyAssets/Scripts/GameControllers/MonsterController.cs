﻿using Assets.Scripts.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GameControllers
{
    public class MonsterController:MonoBehaviour
    {
        private DungeonController DungeonController;
        public List<MonsterObjectMap> MonsterObjectMaps;

        public GameObject GameMonsterCollection;

        private List<GameMonster> Monsters;
        public List<GameObject> GameMonsters { get; set; }

        private void Start()
        {
            DungeonController = GetComponent<DungeonController>();
            Monsters = new List<GameMonster>();
            GameMonsters = new List<GameObject>();
        }

        public void GenerateMonsters(List<GameMonster> monsters)
        {
            Monsters.AddRange(monsters);
            foreach (var monster in monsters)
            {
                GameObject gameMonster = Instantiate(new List<MonsterObjectMap>(MonsterObjectMaps).Find(mom=>mom.MonsterName.Equals(monster.Name)).GameObject);
                gameMonster.transform.position = new Vector3(monster.Position.X, 0f, -monster.Position.Y);
                gameMonster.name = monster.Id.ToString();
                gameMonster.transform.SetParent(GameMonsterCollection.transform, true);
                gameMonster.GetComponent<CharacterModel>().GameMonster = monster;
                gameMonster.GetComponent<CharacterModel>().TileModel = DungeonController.GameTiles
                    .Find(t => t.name.Equals(monster.Position.Id.ToString())).GetComponent<TileModel>();
                GameMonsters.Add(gameMonster);
            }

        }

        public GameObject FindGameMonsterByNameId(long targetId)
        {
            return GameMonsters.Find(gm => gm.name.Equals(targetId.ToString()));
        }

        public GameObject GetMonsterByName(string name)
        {
            return GameMonsterCollection.transform.Find(name).gameObject;
        }

        public void DamageGameMonster(long targetId, int damage, bool dead)
        {
            GameObject gameMonster = FindGameMonsterByNameId(targetId);
            gameMonster.GetComponent<CharacterModel>().GameMonster.HitPoints -= damage;
            gameMonster.transform.Find("HealthBar").Find("Image").GetComponent<Image>().fillAmount =
                (float)gameMonster.GetComponent<CharacterModel>().GameMonster.HitPoints /
                gameMonster.GetComponent<CharacterModel>().GameMonster.MaxHitPoints;
            if (dead || gameMonster.GetComponent<CharacterModel>().GameMonster.HitPoints <= 0)
            {
                DeathOfGameMonster(gameMonster);
            }
        }

        public void DeathOfGameMonster(GameObject gameMonster)
        {
            gameMonster.GetComponent<Animator>().SetTrigger("Die");
        }


    }

    [Serializable]
    public class MonsterObjectMap
    {
        public string MonsterName;
        public GameObject GameObject;
    }
}
