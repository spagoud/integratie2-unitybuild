﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HeroMouseResponse : MonoBehaviour, IPointerClickHandler
{
    private UiController UiController;

    void Start()
    {
        UiController = GameObject.FindGameObjectWithTag("GameController").GetComponent<UiController>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        UiController.EnableHeroInfo(true);
        UiController.SetHeroInfoDisplay(GetComponent<CharacterModel>().GameHero);
    }
}
