﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public string Face;
    public Material DoorMaterial;

    private Transform DoorObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnValidate()
    {
        if (Face != null)
        {
            DoorObject = transform.Find(Face);
            if (DoorObject != null && DoorObject!=transform)
            {
                DoorObject.gameObject.GetComponent<Renderer>().material = DoorMaterial;
            }
        }
    }
}
