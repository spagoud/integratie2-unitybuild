﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameRoomSceneTypes
{
    NOGAME, INGAME, GAMESETUP
}

public class GameRoomSceneLoader : MonoBehaviour
{
    private static GameRoomSceneLoader instance;
    public static GameRoomSceneLoader GetInstance()
    {
        return instance;
    }

    public List<GameRoomSceneLink> scenes;

    private string currentSceneName = "";

    public void Start()
    {
        instance = this;
#if UNITY_WEBGL
        SwitchToScene(GameRoomSceneTypes.INGAME);
#endif

    }

    public void SwitchToScene(GameRoomSceneTypes sceneType)
    {
        var nextSceneName = scenes.Find(gameRoomScene => gameRoomScene.scenetype == sceneType).sceneName;
        if (!String.IsNullOrWhiteSpace(currentSceneName))
            SceneManager.UnloadSceneAsync(currentSceneName);
        SceneManager.LoadSceneAsync(nextSceneName, LoadSceneMode.Additive);
        currentSceneName = nextSceneName;
    }
}

[Serializable]
public class GameRoomSceneLink
{
    public GameRoomSceneTypes scenetype;
    public string sceneName;
}
