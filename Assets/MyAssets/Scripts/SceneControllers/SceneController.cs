﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    private static SceneController instance;
    public static SceneController GetInstance()
    {
        return instance;
    }


    public List<string> mobileSceneNamesOrdered;
    public List<string> webSceneNamesOrdered;

    private string currentSceneName;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
#if UNITY_WEBGL
        currentSceneName=webSceneNamesOrdered[0];
#elif UNITY_EDITOR || UNITY_IOS || UNITY_ANDROID
        currentSceneName = mobileSceneNamesOrdered[0];
#endif
        if (string.IsNullOrEmpty(currentSceneName))
        {
            throw new System.Exception("No scene available for the current platform!");
        }
        SceneManager.LoadScene(currentSceneName, LoadSceneMode.Additive);

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadNextScene()
    {
        var nextSceneName = "";
#if UNITY_WEBGL

        var index = webSceneNamesOrdered.IndexOf(currentSceneName);
        nextSceneName = webSceneNamesOrdered[index++];
#elif UNITY_EDITOR || UNITY_IOS || UNITY_ANDROID

        var index = mobileSceneNamesOrdered.IndexOf(currentSceneName);
        nextSceneName = mobileSceneNamesOrdered[index++];
#endif

        SceneManager.UnloadSceneAsync(currentSceneName);
        SceneManager.LoadSceneAsync(nextSceneName);
        currentSceneName = nextSceneName;
    }


    public void LoadPreviousScene()
    {
        var nextSceneName = "";
#if UNITY_WEBGL

        var index = webSceneNamesOrdered.IndexOf(currentSceneName);
        nextSceneName = webSceneNamesOrdered[index--];
#elif UNITY_EDITOR || UNITY_IOS || UNITY_ANDROID

        var index = mobileSceneNamesOrdered.IndexOf(currentSceneName);
        nextSceneName = mobileSceneNamesOrdered[index--];
#endif

        SceneManager.UnloadSceneAsync(currentSceneName);
        SceneManager.LoadSceneAsync(nextSceneName);
        currentSceneName = nextSceneName;
    }
}
