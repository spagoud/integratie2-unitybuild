﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.GameControllers;
using UnityEngine;
using UnityEngine.EventSystems;

public class MonsterMouseResponse : MonoBehaviour, IPointerClickHandler
{
    private UiController UiController;
    private AttackController AttackController;
    
    void Start()
    {
        UiController = GameObject.FindGameObjectWithTag("GameController").GetComponent<UiController>();
        AttackController = GameObject.FindGameObjectWithTag("GameController").GetComponent<AttackController>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        AttackController.SetTargetId(GetComponent<CharacterModel>().GameMonster.Id);
        bool enableAttack = false;
        bool canAttack = AttackController.CanHeroAttackMonster(transform.position,
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().UserGameHero.transform
                .position);
        bool isTurn = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().IsTurn();
        if (canAttack && isTurn)
        {
            enableAttack = true;
        }
        UiController.EnableMonsterInfo(true);
        UiController.SetMonsterDisplay(GetComponent<CharacterModel>().GameMonster, enableAttack);
        
    }
}
