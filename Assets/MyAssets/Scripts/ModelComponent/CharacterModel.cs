﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model;
using UnityEngine;

public class CharacterModel : MonoBehaviour
{
    public GameHero GameHero { get; set; }
    public GameMonster GameMonster { get; set; }
    public TileModel TileModel { get; set; }
}