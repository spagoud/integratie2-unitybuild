﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model;
using UnityEngine;

public class TileModel : MonoBehaviour
{
    public Tile Tile { get; set; }
    public DoorModel DoorModel { get; set; }
    public GameObject ChestGameObject { get; set; }
}
