﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.PlayerLoop;

public class HoverResponse : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private UiController UiController;

    void Start()
    {
        UiController = GameObject.FindGameObjectWithTag("GameController").GetComponent<UiController>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        UiController.EnableTreasureInfo(true);
        UiController.SetTreasureDisplay(GetComponent<ItemModel>().Item);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        UiController.EnableTreasureInfo(false);
        transform.Find("OverLay").gameObject.SetActive(false);
    }
}